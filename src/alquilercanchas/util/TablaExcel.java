/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.util;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class TablaExcel {
    private SimpleStringProperty nombre;
    private SimpleStringProperty fechaIni;
    private SimpleStringProperty fechaCorte;
    private SimpleStringProperty ganancias;
    private SimpleStringProperty jugados;
    private SimpleStringProperty noJugados;
    
    public TablaExcel(){
        this.nombre = new SimpleStringProperty("");
        this.fechaIni = new SimpleStringProperty("");
        this.fechaCorte = new SimpleStringProperty("");
        this.ganancias = new SimpleStringProperty("");
        this.jugados = new SimpleStringProperty("");
        this.noJugados = new SimpleStringProperty("");
    }
    
    public TablaExcel(String nombre, String fechaIni, String fechaCorte, String ganancias, String jugados, String noJugados){
        this.nombre = new SimpleStringProperty(nombre);
        this.fechaIni = new SimpleStringProperty(fechaIni);
        this.fechaCorte = new SimpleStringProperty(fechaCorte);
        this.ganancias = new SimpleStringProperty(ganancias);
        this.jugados = new SimpleStringProperty(jugados);
        this.noJugados = new SimpleStringProperty(noJugados);
    }
    
    public String getNombre(){
        return nombre.get();
    }
    
    public void setNombre(String nombre){
        this.nombre.set(nombre);
    }
    
    public String getFechaIni(){
        return fechaIni.get();
    }
    
    public void setFechaIni(String fechaIni){
        this.fechaIni.set(fechaIni);
    }
    
    public String getFechaCorte(){
        return fechaCorte.get();
    }
    
    public void setFechaCorte(String fechaCorte){
        this.fechaCorte.set(fechaCorte);
    }
    
    public String getGanancias(){
        return ganancias.get();
    }
    
    public void setGanancias(String ganancias){
        this.ganancias.set(ganancias);
    }
    
    public String getJugados(){
        return jugados.get();
    }
    
    public void setJugados(String jugados){
        this.jugados.set(jugados);
    }
    
    public String getNoJugados(){
        return noJugados.get();
    }
    
    public void setNoJugados(String noJugados){
        this.noJugados.set(noJugados);
    }
}
