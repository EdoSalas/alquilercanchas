/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.util;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.time.Clock;
import java.time.LocalTime;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class Exportar {

    //Atributos
    private String ruta;
    private ArrayList<String> encabezados;

    //Atributos de Excel
    private File excel;
    private HSSFWorkbook libro;
    private FileOutputStream archivo;
    private HSSFSheet hoja;
    private HSSFFont fontEncabezado;
    private HSSFFont fontCeldas;
    private HSSFCellStyle styleEncabezado;
    private HSSFCellStyle styleCeldas;

    //Constructores
    public Exportar() {
        this.ruta = "Datos.xls";
        this.excel = new File(this.ruta);
        try {
            this.excel.createNewFile();
            this.libro = new HSSFWorkbook();
            this.archivo = new FileOutputStream(this.excel);
            this.hoja = this.libro.createSheet("Datos");
            estilos();
            encabezado();
        } catch (Exception e) {
            Funciones.mensajeError("No se puede generar el excel");
        }

    }

    public void estilos() {
        //Fuentes
        this.fontEncabezado = this.libro.createFont();
        this.fontEncabezado.setBold(true);
        this.fontEncabezado.setColor(HSSFColor.HSSFColorPredefined.DARK_BLUE.getIndex());
        this.fontEncabezado.setFontName(HSSFFont.FONT_ARIAL);

        this.fontCeldas = this.libro.createFont();
        this.fontCeldas.setBold(false);
        this.fontCeldas.setColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        this.fontCeldas.setFontName(HSSFFont.FONT_ARIAL);
        this.fontCeldas.setItalic(true);

        //Estilos
        this.styleEncabezado = this.libro.createCellStyle();
        this.styleEncabezado.setFillForegroundColor(HSSFColor.HSSFColorPredefined.AQUA.getIndex());
        this.styleEncabezado.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        this.styleEncabezado.setAlignment(HorizontalAlignment.CENTER);
        this.styleEncabezado.setFont(this.fontEncabezado);
        this.styleEncabezado.setBorderTop(BorderStyle.DOUBLE);
        this.styleEncabezado.setBorderRight(BorderStyle.DOUBLE);
        this.styleEncabezado.setBorderLeft(BorderStyle.DOUBLE);
        this.styleEncabezado.setBorderBottom(BorderStyle.DOUBLE);

        this.styleCeldas = this.libro.createCellStyle();
        this.styleCeldas.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        this.styleEncabezado.setFillBackgroundColor(HSSFColor.HSSFColorPredefined.GREY_50_PERCENT.getIndex());
        this.styleCeldas.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        this.styleCeldas.setFont(this.fontCeldas);
        this.styleCeldas.setBorderLeft(BorderStyle.MEDIUM);
        this.styleCeldas.setBorderRight(BorderStyle.MEDIUM);
        this.styleCeldas.setBorderBottom(BorderStyle.MEDIUM);
    }

    public void encabezado() {
        this.encabezados = new ArrayList();
        this.encabezados.add("Cancha");
        this.encabezados.add("Fecha Inicial");
        this.encabezados.add("Fecha Corte");
        this.encabezados.add("Ganancias Generadas");
        this.encabezados.add("Partidos Jugados");
        this.encabezados.add("Partidos no Jugados");
        this.encabezados.add("Hora del Reporte");
    }

    public void exporta(ObservableList<TablaExcel> tablaExcel) {
        try {
            //Encabezados
            for (int enc = 0; enc < tablaExcel.size() + 1; enc++) {
                HSSFRow fila = this.hoja.createRow(enc);
                //Celdas
                for (int cel = 0; cel < this.encabezados.size(); cel++) {
                    HSSFCell celda = fila.createCell(cel);
                    if (enc == 0) {
                        celda.setCellStyle(this.styleEncabezado);
                        celda.setCellValue(this.encabezados.get(cel));
                        this.hoja.autoSizeColumn(cel); //Ajustar al texto
                    } else {
                        celda.setCellStyle(this.styleCeldas);
                        switch (cel) {
                            case 0:
                                celda.setCellValue(tablaExcel.get(enc-1).getNombre());
                                break;
                            case 1:
                                celda.setCellValue(tablaExcel.get(enc-1).getFechaIni());
                                break;
                            case 2:
                                celda.setCellValue(tablaExcel.get(enc-1).getFechaCorte());
                                break;
                            case 3:
                                    celda.setCellValue(tablaExcel.get(enc-1).getGanancias());
                                break;
                            case 4:
                                celda.setCellValue(tablaExcel.get(enc-1).getJugados());
                                break;
                            case 5:
                                celda.setCellValue(tablaExcel.get(enc-1).getNoJugados());
                                break;
                            case 6:
                                celda.setCellValue(LocalTime.now(Clock.systemUTC()).toString());
                                break;
                            default:
                                break;
                        }
                        this.hoja.autoSizeColumn(cel);
                    }
                }
            }
            this.libro.write(this.archivo);
            this.archivo.close();
            Desktop.getDesktop().open(this.excel);
        } catch (Exception e) {
            Funciones.mensajeAdvertencia("No existen datos para exportar a Excel");
        }
    }
}
