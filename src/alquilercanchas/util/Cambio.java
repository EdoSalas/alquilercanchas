package alquilercanchas.util;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 *
 * @author jocser
 */
public class Cambio {

    private ScaleTransition st;
    private ScaleTransition st2;
    private int n;

    public Cambio() {
        this.n = 1;
    }
    /**
     * desaparicion de la pantalla
     * @param iv3
     */
    public void aniCamPantallaInicial( VBox vBox) {
        FadeTransition ft = new FadeTransition(Duration.millis(100), vBox);
        ft.setDelay(Duration.seconds(1));
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.play();
       
    }
    /**
     * Animacion de aparicion de la pantalla
     * @param vBox 
     */
     public void aniCamPantallaFinal( VBox vBox) {
            FadeTransition ft2 = new FadeTransition(Duration.millis(550), vBox);
            ft2.setFromValue(0);
            ft2.setToValue(1);
            ft2.play();
         
     }
   /**
    * animacion para cargar las imagenes
    * @param im
    * @return 
    */
    public Boolean cargarImagen(ImageView im){
     
        if (n == 1) {
            this.st = new ScaleTransition(Duration.millis(200), im);
            this.st.setFromY(1.0);
            this.st.setToY(0.0);
            this.st.play();
            this.st.setOnFinished((event) -> {
                this.st2 = new ScaleTransition(Duration.millis(200), im);
                this.st2.setFromY(0.0);
                this.st2.setToY(1.0);
                this.st2.play();
            });
            //para cambiar de animacion depende del cambio de carta
            n = 2;
        } else {

            this.st = new ScaleTransition(Duration.millis(200), im);
            this.st.setFromX(1.0);
            this.st.setToX(0.0);
            this.st.play();
            this.st.setOnFinished((event) -> {
                this.st2 = new ScaleTransition(Duration.millis(200), im);
                this.st2.setFromX(0.0);
                this.st2.setToX(1.0);
                this.st2.play();
            });
            n = 1;
        }
        return true;
    }
      
}
