/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.util;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class TablaInfo {
    private SimpleStringProperty cancha;
    private SimpleStringProperty equipo;
    private SimpleStringProperty goles;
    private SimpleStringProperty golesContrincante;
    private SimpleStringProperty contrincante;
    private SimpleStringProperty puntaje;
    
    public TablaInfo(){
        this.cancha = new SimpleStringProperty();
        this.equipo = new SimpleStringProperty();
        this.goles = new SimpleStringProperty();
        this.golesContrincante = new SimpleStringProperty();
        this.contrincante = new SimpleStringProperty();
        this.puntaje = new SimpleStringProperty();
    }

    public TablaInfo(String cancha, String equipo, String goles, String golesCont, String cont, String puntaje){
        this.cancha = new SimpleStringProperty(cancha);
        this.equipo = new SimpleStringProperty(equipo);
        this.goles = new SimpleStringProperty(goles);
        this.golesContrincante = new SimpleStringProperty(golesCont);
        this.contrincante = new SimpleStringProperty(cont);
        this.puntaje = new SimpleStringProperty(puntaje);
    }
    
    public String getCancha(){
        return this.cancha.get();
    }
    
    public void setCancha(String cancha){
        this.cancha.set(cancha);
    }
    
    public String getEquipo(){
        return this.equipo.get();
    }
    
    public void setEquipo(String equipo){
        this.equipo.set(equipo);
    }
    
    public String getGoles(){
        return this.goles.get();
    }
    
    public void setGoles(String goles){
        this.goles.set(goles);
    }
    
    public String getGolesCont(){
        return this.golesContrincante.get();
    }
    
    public void setGolesCont(String golesCont){
        this.golesContrincante.set(golesCont);
    }
    
    public String getCont(){
        return this.contrincante.get();
    }
    
    public void setCont(String cont){
        this.contrincante.set(cont);
    }
    
    public String getPuntaje(){
        return this.puntaje.get();
    }
    
    public void setPuntaje(String puntaje){
        this.puntaje.set(puntaje);
    }
}
