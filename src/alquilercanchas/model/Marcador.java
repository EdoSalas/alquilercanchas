/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.model;

import cr.ac.una.canchasws.controller.MarcadorDto;
import java.util.ArrayList;

/**
 *
 * @author liedu
 */
public class Marcador {

    private Integer golesEqui1;
    private Integer golesEqui2;
    private ArrayList<Equipo> esquipos = new ArrayList();
    public Marcador() {
        golesEqui1 = 0;
        golesEqui2 = 0;
    }

    public Marcador( Integer golesEqui1, Integer golesEqui2) {
        this.golesEqui1 = golesEqui1;
        this.golesEqui2 = golesEqui2;
    }

    public Marcador(MarcadorDto m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getGolesEqui1() {
        return golesEqui1;
    }

    public void setGolesEqui1(Integer golesEqui1) {
        this.golesEqui1 = golesEqui1;
    }

    public Integer getGolesEqui2() {
        return golesEqui2;
    }

    public void setGolesEqui2(Integer golesEqui2) {
        this.golesEqui2 = golesEqui2;
    }

    public ArrayList<Equipo> getEsquipos() {
        return esquipos;
    }

    public void setEsquipos(ArrayList<Equipo> esquipos) {
        this.esquipos = esquipos;
    }

}