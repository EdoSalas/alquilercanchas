/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.model;

import cr.ac.una.canchasws.controller.ContactoDto;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Matami
 */
public class Contacto {
    public SimpleStringProperty nombre;
    public SimpleStringProperty telefono;

    public Contacto() {
        nombre = new SimpleStringProperty();
        telefono = new SimpleStringProperty();
    }

    public Contacto(String nombre, String telefono) {
        this();
        this.nombre.set(nombre);
        this.telefono.set(telefono);
    }

    public Contacto(ContactoDto cont) {
        this();
        nombre.set(cont.getNombre());
        telefono.set(cont.getTelefono().toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getTelefono() {
        return telefono.get();
    }

    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    @Override
    public String toString() {
        return "Contactos{" + "nombre=" + nombre + ", telefono=" + telefono + '}';
    }
    
    
}
