/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.model;

import alquilercanchas.util.AppContext;
import alquilercanchas.util.FlowController;
import com.jfoenix.controls.JFXButton;
import cr.ac.una.canchasws.controller.EquipoDto;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Matami
 */
public class Equipo {

    public ObjectProperty<Image>  Bandera;
    public SimpleStringProperty nombre;
    public SimpleStringProperty puntos;
    public SimpleStringProperty rendimiento;
    public SimpleStringProperty goles;
    public JFXButton boton;
    private Usuario1 capitan;//nombre del capitan 
    private  ObservableList<Contacto> integrantes = FXCollections.observableArrayList();
    private Equipo infoEquipo;

    public Equipo() {
        this.Bandera = new SimpleObjectProperty();
        nombre = new SimpleStringProperty();
        puntos = new SimpleStringProperty();
        rendimiento = new SimpleStringProperty();
        goles = new SimpleStringProperty("0");
        boton = new JFXButton();
        this.setPuntos(0);
    }

    public Equipo(String nombre, Integer puntos, Float rendimiento, JFXButton botn) {
        this();
        this.nombre.set(nombre);
        this.puntos.set(puntos.toString());
        this.rendimiento.set(rendimiento.toString());
        this.boton = botn;
        this.boton.setOnMouseClicked((MouseEvent e) -> {
            infoEquipo = this;
            AppContext.getInstance().set("equipoInfo", this.infoEquipo);
            FlowController.getInstance().delete("InfoEquipo");
            FlowController.getInstance().goView("InfoEquipo");
        });
    }

    public Equipo(EquipoDto equipo) {
        this();
        this.nombre.set(equipo.getNombre());
        this.puntos.set(equipo.getPuntos().toString());
        this.rendimiento.set(equipo.getRendimiento().toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public Integer getPuntos() {
        if(puntos.get() != null && !puntos.get().isEmpty()){
            return Integer.valueOf(puntos.get());
        }
        else{
            return null;
        }
    }

    public void setPuntos(Integer puntos) {
        this.puntos.set(puntos.toString());
    }

    public Float getRendimiento() {
        if(rendimiento.get()!=null&&!rendimiento.get().isEmpty()){
            return Float.valueOf(rendimiento.get());
        }
        else{
            return null;
        }
    }

    public void setRendimiento(Float rendimiento) {
        this.rendimiento.set(rendimiento.toString());
    }

    public Usuario1 getCapitan() {
        return capitan;
    }

    public void setCapitan(Usuario1 capitan) {
        this.capitan = capitan;
    }

    public ObservableList<Contacto> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(ObservableList<Contacto> integrantes) {
        this.integrantes = integrantes;
    }

    public Image getBandera() {
        return Bandera.get();
    }

    public void setBandera(Image Bandera) {
        this.Bandera.set(Bandera);
    }

    public Integer getGoles() {
        if(rendimiento.get()!=null&&!rendimiento.get().isEmpty()){
            return Integer.valueOf(goles.get());
        }
        else{
            return null;
        }
    }

    public void setGoles(Integer goles) {
        this.goles.set(goles.toString());
    }

    public JFXButton getBoton() {
        return boton;
    }

    public void setBoton(JFXButton boton) {
        this.boton = boton;
    }
    
    @Override
    public String toString() {
        return "Equipo{" + "nombre=" + nombre + ", puntos=" + puntos + ", rendimiento=" + rendimiento + ", integrantes=" + integrantes + '}';
    }
    
    
}
