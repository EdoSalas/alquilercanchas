/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.model;

import cr.ac.una.canchasws.controller.UsuarioDto;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class Usuario1 {

    //Atributos
    public SimpleStringProperty id;
    public ObjectProperty<String> tipoUsuario;
    public SimpleStringProperty nombre;
    public SimpleStringProperty usuario;
    public SimpleStringProperty clave;
    public SimpleStringProperty nombreEquipo;
    public SimpleStringProperty datosCompletos;//"0" no completos "1" completos
    private String activo;
    private Equipo equipo;
    private ObservableList<Cancha> canchas = FXCollections.observableArrayList();

    //Constructores
    public Usuario1(){
        this.id = new SimpleStringProperty();
        this.tipoUsuario = new SimpleObjectProperty("E");
        this.nombre = new SimpleStringProperty("");
        this.usuario = new SimpleStringProperty("");
        this.clave = new SimpleStringProperty("");
        this.nombreEquipo = new SimpleStringProperty("");
        this.datosCompletos = new SimpleStringProperty("0");
    }

    public Usuario1(UsuarioDto us) {
        this();
        setId(us.getId());
        this.setTipoUsuario(us.getTipoUsuario());
//        this.nombre.set(nombre);
        this.setUsuario(us.getUsuario());
        this.setClave(us.getClave());
        this.setNombreEquipo(us.getNombreEquipo());
        this.activo = us.getActivo();
    }

    public Long getId() {
       if(id.get()!=null&&!id.get().isEmpty()){
           return Long.valueOf(id.get());
       }else{
           return null;
       }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }
    
    public String getTipoUsuario() {
        return tipoUsuario.get();
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario.set(tipoUsuario);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getUsuario() {
        return usuario.get();
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    public String getClave() {
        return clave.get();
    }

    public void setClave(String clave) {
        this.clave.set(clave);
    }

    public String getNombreEquipo() {
        return nombreEquipo.get();
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo.set(nombreEquipo);
    }

    public Integer getDatosCompletos() {
        if(datosCompletos.get()!=null&&!datosCompletos.get().isEmpty()){
            return Integer.valueOf(datosCompletos.get());
        }
        else{
            return null;
        }
    }

    public void setDatosCompletos(Integer datosCompletos) {
        this.datosCompletos.set(datosCompletos.toString());
    }
    
    

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public ObservableList<Cancha> getCanchas() {
        return canchas;
    }

    public void setCanchas(ObservableList<Cancha> canchas) {
        this.canchas = canchas;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", tipoUsuario=" + tipoUsuario + ", nombre=" + nombre + ", usuario=" + usuario + ", clave=" + clave + ", nombreEquipo=" + nombreEquipo + ", datosCompletos=" + datosCompletos + ", activo=" + activo + ", equipo=" + equipo + ", canchas=" + canchas + '}';
    }
     
}
