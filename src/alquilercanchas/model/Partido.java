/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.model;

import alquilercanchas.util.AppContext;
import com.jfoenix.controls.JFXButton;
import cr.ac.una.canchasws.controller.LocalDate;
import cr.ac.una.canchasws.controller.LocalTime;
import cr.ac.una.canchasws.controller.PartidoDto;
import java.time.ZoneId;
import java.util.ArrayList;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Eduardo
 */
public class Partido extends JFXButton {

    public ObjectProperty<LocalTime> hora;
    public ObjectProperty<LocalDate> dia;
    public SimpleBooleanProperty estado;
    public SimpleBooleanProperty finalizado;
    private Marcador marcador;
    private Equipo ganador;
    private Boolean empate;
    private ArrayList<Equipo> equipos;
    private Cancha cancha;
    private ObservableList<Partido> obPartido;

    public Partido() {
        hora = new SimpleObjectProperty();
        dia = new SimpleObjectProperty();
        estado = new SimpleBooleanProperty();
        finalizado = new SimpleBooleanProperty();
        equipos = new ArrayList();
        empate=Boolean.FALSE;
        marcador = new Marcador();
        this.obPartido = FXCollections.observableArrayList();
    }
      public Partido(PartidoDto par) {
        this();
        hora.set(par.getHora());
        dia.set(par.getDia());
        serEstadoConver(par.getEstado());
        serFinConver(par.getFinalizado());
    }
    public Partido(LocalTime hora, LocalDate dia, Boolean estado, ArrayList<Equipo> equipos) {
        this.hora.set(hora);
        this.dia.set(dia);
        this.estado.set(estado);
        this.equipos = equipos;
    }
    public void serEstadoConver(String es){
        if(es.equalsIgnoreCase("I")){
            estado.set(false);
        }else{
            estado.set(true);
        }
    }
     public void serFinConver(String es){
        if(es.equalsIgnoreCase("N")){
            estado.set(false);
        }else{
            estado.set(true);
        }
    }
    /**
     * Este constructor es para la exportación a Excel
     *
     * @param cancha
     * @param lt LocalTime
     * @param ld LocalDate
     */
    public Partido(Cancha cancha, LocalTime lt, LocalDate ld) {
        this.cancha = cancha;
        this.hora.set(lt);
        this.dia.set(ld);
    }
    
    public LocalTime getHora() {
        return hora.get();
    }

    public void setHora(LocalTime hora) {
        this.hora.set(hora);
    }

    public LocalDate getDia() {
        return dia.get();
    }

    public void setDia(LocalDate dia) {
        this.dia.set(dia);
    }

    public Boolean getEstado() {
        return estado.get();
    }

    public void setEstado(Boolean estado) {
        this.estado.set(estado);
    }

    public ArrayList<Equipo> getEquipos() {
        return equipos;
    }

    public void setEquipos(ArrayList<Equipo> equipos) {
        this.equipos = equipos;
    }

    public Cancha getCancha() {
        return cancha;
    }

    public void setCancha(Cancha cancha) {
        this.cancha = cancha;
    }

    public ObservableList<Partido> getObPartido() {
        return obPartido;
    }

    public void setObPartido(ObservableList<Partido> obPartido) {
        this.obPartido = obPartido;
    }

    public Marcador getMarcador() {
        return marcador;
    }

    public void setMarcador(Marcador marcador) {
        this.marcador = marcador;
    }

    public Boolean getFinalizado() {
        return finalizado.get();
    }

    public void setFinalizado(Boolean finalizado) {
        this.finalizado.set(finalizado);
    }

    public Equipo getGanador() {
        return ganador;
    }

    public void setGanador(Equipo ganador) {
        this.ganador = ganador;
    }

    public Boolean getEmpate() {
        return empate;
    }

    public void setEmpate(Boolean empate) {
        this.empate = empate;
    }
    
    public Boolean getEsta(Equipo equipo){
        if(equipo.equals(equipos.get(0))){
            return true;
        }else if(equipo.equals(equipos.get(1))){
            return true;
        }else{
            return false;
        }
    }
    
    public Equipo getLocal(Equipo equipo){
        if(equipo.equals(equipos.get(0))){
            return equipos.get(0);
        }else if(equipo.equals(equipos.get(1))){
            return equipos.get(1);
        }else{
            return null;
        }
    }
    
    public Equipo getContrincante(Equipo equipo){
        if(!equipo.equals(equipos.get(0))){
            return equipos.get(0);
        }else if(!equipo.equals(equipos.get(1))){
            return equipos.get(1);
        }else{
            return null;
        }
    }

    @Override
    public String toString() {
        return "Partido{" + "hora=" + hora + ", dia=" + dia + ", estado=" + estado + ", equipos=" + equipos + '}';
    }
}//fin clase
