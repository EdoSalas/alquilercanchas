/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.model;

import cr.ac.una.canchasws.controller.CanchaDto;
import cr.ac.una.canchasws.controller.LocalTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Matami
 */
public class Cancha {

    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public ObjectProperty<LocalTime> abierto;
    public ObjectProperty<LocalTime> cerrado;
    public SimpleStringProperty precioDia;
    public SimpleStringProperty precioNoche;
    public SimpleStringProperty numeroJugadores;
    public SimpleStringProperty numeroTelefono;
    public SimpleStringProperty titulo;
    public SimpleStringProperty direccion;
    public SimpleStringProperty lat;
    public SimpleStringProperty lon;
    private Usuario1 usuario;

    public Cancha() {
        id = new SimpleStringProperty();
        nombre = new SimpleStringProperty();
        abierto = new SimpleObjectProperty();
        cerrado = new SimpleObjectProperty();
        precioDia = new SimpleStringProperty();
        precioNoche = new SimpleStringProperty();
        numeroJugadores = new SimpleStringProperty();
        numeroTelefono = new SimpleStringProperty();
        titulo = new SimpleStringProperty();
        direccion = new SimpleStringProperty();
        lat = new SimpleStringProperty();
        lon = new SimpleStringProperty();
    }

    public Cancha(String nombre, LocalTime abierto, LocalTime cerrado, Integer precioDia, Integer precioNoche, Integer numeroJugadores, String numero, String titulo, String descripcion, String latitud, String longitud) {
        this();
        this.nombre.set(nombre);
        this.abierto.set(abierto);
        this.cerrado.set(cerrado);
        this.precioDia.set(precioDia.toString());
        this.precioNoche.set(precioNoche.toString());
        this.numeroJugadores.set(numeroJugadores.toString());
        this.numeroTelefono.set(numero);
        this.titulo.set(titulo);
        this.direccion.set(descripcion);
        this.lat.set(latitud);
        this.lon.set(longitud);
    }

    public Cancha(CanchaDto can) {
        this();
        setNombre(can.getNombre());
        setAbierto(can.getAbierto());
        setCerrado(can.getCerrado());
        setPrecioDia(can.getPrecioDia());
        setPrecioNoche(can.getPrecioNoche());
        setNumeroJugadores(can.getNumeroJugadores());
        setNumeroTelefono(can.getNumeroTelefono());
        setTitulo(can.getTitulo());
        setDireccion(can.getDireccion());
        setLat(can.getLat().toString());
        setLon(can.getLon().toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public LocalTime getAbierto() {
        return abierto.get();
    }

    public void setAbierto(LocalTime abierto) {
        this.abierto.set(abierto);
    }

    public LocalTime getCerrado() {
        return cerrado.get();
    }

    public void setCerrado(LocalTime cerrado) {
        this.cerrado.set(cerrado);
    }

    public Integer getPrecioDia() {
        if (precioDia.get() != null && !precioDia.get().isEmpty()) {
            return Integer.valueOf(precioDia.get());
        } else {
            return null;
        }
    }

    public void setPrecioDia(Integer precioDia) {
        this.precioDia.set(precioDia.toString());
    }

    public Integer getPrecioNoche() {
        if (precioNoche.get() != null && !precioNoche.get().isEmpty()) {
            return Integer.valueOf(precioNoche.get());
        } else {
            return null;
        }
    }

    public void setPrecioNoche(Integer precioNoche) {
        this.precioNoche.set(precioNoche.toString());
    }

    public Integer getNumeroJugadores() {
        if (numeroJugadores.get() != null && !numeroJugadores.get().isEmpty()) {
            return Integer.valueOf(numeroJugadores.get());
        } else {
            return null;
        }
    }

    public void setNumeroJugadores(Integer numeroJugadores) {
        this.numeroJugadores.set(numeroJugadores.toString());
    }

    public Usuario1 getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario1 usuario) {
        this.usuario = usuario;
    }

    public Integer getNumeroTelefono() {
       if( numeroTelefono.get()!=null&&!numeroTelefono.get().isEmpty()){
           return Integer.valueOf(numeroTelefono.get());
       }else{
           return null;
       }
    }

    public void setNumeroTelefono(Integer numeroTelefono) {
        this.numeroTelefono.set(numeroTelefono.toString());
    }

    public String getDireccion() {
        return direccion.get();
    }

    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }

    public String getLat() {
        return lat.get();
    }

    public void setLat(String lat) {
        this.lat.set(lat);
    }

    public String getLon() {
        return lon.get();
    }

    public void setLon(String lon) {
        this.lon.set(lon);
    }

    public String getTitulo() {
        return titulo.get();
    }

    public void setTitulo(String titulo) {
        this.titulo.set(titulo);
    }

    @Override
    public String toString() {
        return (this.nombre.get() + " " + this.numeroTelefono.get());
    }

}
