/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Equipo;
import alquilercanchas.util.AppContext;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class RankingController extends Controller implements Initializable {

    @FXML
    private Label lbRanking;
    @FXML
    private TableView<Equipo> tvRanking;
    @FXML
    private TableColumn<Equipo, SimpleStringProperty> tcNombre;
    @FXML
    private TableColumn<Equipo, SimpleStringProperty> tcPuntos;
    @FXML
    private TableColumn<Equipo, SimpleStringProperty> tcRendimiento;
    @FXML
    private TableColumn<Equipo, String> clBotones;

    //Atributos
    private  ObservableList<Equipo> equipos;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       inicializarComponentes();
    }    
    public void inicializarComponentes(){
        tcNombre.setText("Nombre");
        tcPuntos.setText("Puntos");
        lbRanking.setText("Ranking");
        tcRendimiento.setText("Rendimiento");
        clBotones.setText("Ir");
        tcNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tcPuntos.setCellValueFactory(new PropertyValueFactory<>("puntos"));
        tcRendimiento.setCellValueFactory(new PropertyValueFactory<>("rendimiento"));
        clBotones.setCellValueFactory(new PropertyValueFactory<>("boton"));
        //listas
        this.equipos = (ObservableList<Equipo>) AppContext.getInstance().get("equipos");
        acomodar();
        this.tvRanking.setItems(equipos);
    }
    @Override
    public void initialize() {
        llenaTabla();
    }
    /**
     * ordena la lista 
     */
    
    public void llenaTabla(){
        acomodar();
        this.tvRanking.setItems(this.equipos);
        this.tvRanking.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    } 
    
    public void acomodar(){
        if(equipos.size()>0){
            Collections.sort(equipos, (Equipo p1, Equipo p2) -> p1.getPuntos().compareTo(p2.getPuntos())); 
        }
    }
}
