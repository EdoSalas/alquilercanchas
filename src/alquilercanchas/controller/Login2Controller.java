/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.util.AppContext;
import alquilercanchas.util.FlowController;
import alquilercanchas.util.Mensaje;
import alquilercanchas.model.Usuario1;
import alquilercanchas.service.UsuarioService;
import alquilercanchas.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class Login2Controller extends Controller implements Initializable {

    @FXML
    private ImageView imgLogo;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private HBox hbClave;
    @FXML
    private JFXButton btnMostrarClave;
    @FXML
    private FontAwesomeIconView icon;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField txtPassword;
    @FXML
    private JFXButton btnIniciar;
    @FXML
    private JFXButton btnRegistro;
    @FXML
    private JFXButton btnLimpiar;

     //Atributos 
    private ArrayList<Usuario1> usuarios;
    
    private Usuario1 usuario;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializarComponentes();
      
    }    

    @Override
    public void initialize() {
        
    }
    
    public void inicializarComponentes(){
        this.btnIniciar.setText("Iniciar");
        this.btnLimpiar.setText("Limpiar");
        this.btnRegistro.setText("Registrarse");
        this.hbClave.getChildren().remove(this.txtPassword);
        this.txtUsuario.setPromptText("Usuario");
        this.txtUsuario.setLabelFloat(true);
        this.password.setPromptText("Clave");
        this.password.setLabelFloat(true);
        this.txtPassword.setPromptText("Clave");
        this.txtPassword.setLabelFloat(true);
        this.usuario = new Usuario1();
        usuarios = (ArrayList<Usuario1>) AppContext.getInstance().get("usuarios");
    }

    @FXML
    private void muestraClave(ActionEvent event) {
        if (this.icon.getGlyphName().equalsIgnoreCase("EYE")) {
            this.hbClave.getChildren().add(this.txtPassword);
            this.txtPassword.setText(this.password.getText());
            this.hbClave.getChildren().remove(this.password);
            this.icon.setGlyphName("EYE_SLASH");
        } else {
            this.hbClave.getChildren().add(this.password);
            this.password.setText(this.txtPassword.getText());
            this.hbClave.getChildren().remove(this.txtPassword);
            this.icon.setGlyphName("EYE");
        }
    }
  
    @FXML
    private void iniciar(ActionEvent event) {
        if(iniciarSesion()){
            ((Stage) btnIniciar.getScene().getWindow()).close();
            FlowController.getInstance().delete("Login2");
             FlowController.getInstance().goMain();
        }
    }
  /**
     * Verifica que el usuario y la clave sean correctos
     * @return 
     */
    public Boolean iniciarSesion() {
            Boolean x=false;
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnLimpiar.getScene().getWindow(), "Debe ingresar un usuario.");
            } else if (password.getText() == null || password.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnLimpiar.getScene().getWindow(), "Debe ingresar una contraseña");
            } else {
                UsuarioService usuarioService = new UsuarioService();
                Respuesta respuesta = usuarioService.getUsuario(txtUsuario.getText(), password.getText());
                if (respuesta.getEstado()) {
                    AppContext.getInstance().set("Usuario", (Usuario1) respuesta.getResultado("Usuario"));
                    System.out.println("Validacion correcta");
                    x=true;
                } else {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(), respuesta.getMensaje());
                    x=false;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Login2Controller.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
        return x;
    }
    /**
     * 
     * @return Lista con el usuario registrado 
     */
    public List<Usuario1> buscar(){
        return usuarios.stream().filter(x->x.getUsuario().equals(txtUsuario.getText())).collect(Collectors.toList());
    }
    /**
     * Veerfica que los campos no esten vacios
     * @return 
     */
    public Boolean camposRequeridos() {
        if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciar.getScene().getWindow(), "Es necesario digitar un usuario para ingresar.");
            return false;
        } else if (password.getText() == null || password.getText().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciar.getScene().getWindow(), "Es necesario digitar la clave para ingresar.");
            return false;
        } else {
            return true;
        }
    }
    @FXML
    private void registro(ActionEvent event) {
        FlowController.getInstance().goViewInStage("Registro", getStage());
        getStage().setWidth(622);
        getStage().setHeight(384);
    }
    /**
     *Limpia los campos para ingresaar nuevos datos
     * @param event 
     */
    @FXML
    private void limpiar(ActionEvent event) {
        this.txtUsuario.setText("");
        this.password.setText("");
        this.txtPassword.setText("");
    }
    
    public HBox getClave(){
        return this.hbClave;
    }
   
}
