/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.util.AppContext;
import alquilercanchas.util.FlowController;
import alquilercanchas.util.Mensaje;
import alquilercanchas.model.Usuario1;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class MenuController extends Controller implements Initializable {

    //Canchas
    @FXML
    private HBox MenuEquipo;
    @FXML
    private JFXButton btnMantenimentoCancha;
    @FXML
    private JFXButton btnInformacionCancha;
    @FXML
    private JFXButton btnRankingCanchas;
    @FXML
    private JFXButton btnLogOutAdmin;
    //Equipos
    @FXML
    private HBox MenuCancha;
    @FXML
    private JFXButton btnMantenimientoEquipo;
    @FXML
    private JFXButton btnInformacionEquipos;
    @FXML
    private JFXButton btnCanchasDisponibles;
    @FXML
    private JFXButton btnRankingEquipo;
    @FXML
    private JFXButton btnLogOutEquipo;

    //Atributos
    private VBox vbPrincipal;

    private Usuario1 usuario;
    @FXML
    private HBox hbMenu;

    //private ArrayList<Usuario1> usuarios;
    @FXML
    private JFXButton btnAgenda;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializaComponentes();
        cargaMenu();

    }

    @Override
    public void initialize() {
    }

    public void inicializaComponentes() {
        //Canchas
        this.btnMantenimentoCancha.setText("Mantenimiento");
        this.btnInformacionCancha.setText("Excel");
        this.btnRankingCanchas.setText("Ranking");
        this.btnAgenda.setText("Agenda");
        this.btnLogOutAdmin.setText("Cerrar Sesión");
        //Equipos
        this.btnMantenimientoEquipo.setText("Mantenimiento");
        this.btnInformacionEquipos.setText("Información");
        this.btnRankingEquipo.setText("Ranking");
        this.btnLogOutEquipo.setText("Cerrar Sesión");
        this.btnCanchasDisponibles.setText("Partido");
        //cargar datos
        // this.usuarios = (ArrayList<Usuario1>) AppContext.getInstance().get("usuarios");

    }

    public void cargaMenu() {
        usuario = (Usuario1) AppContext.getInstance().get("Usuario");
        System.out.println(usuario);
        if (usuario.getTipoUsuario().equals("C")) {
            this.hbMenu.getChildren().remove(this.MenuEquipo);
        } else {
            this.hbMenu.getChildren().remove(this.MenuCancha);
        }
    }

    @FXML
    private void ranking(ActionEvent event) {

        FlowController.getInstance().goView("Ranking");

    }

    @FXML
    private void cerrarSesion(ActionEvent event) {
        if (new Mensaje().showConfirmation("Confirmacion", getStage(), "¿Desea realmente cerrar la sesion?.\n Recuerde guardar los cambios")) {
            if (usuario.getTipoUsuario().equals("C")) {
//               usuario.setActivo(false);
                ((Stage) btnLogOutAdmin.getScene().getWindow()).close();
                FlowController.getInstance().delete("MantenimientoCancha");
                FlowController.getInstance().delete("Principal");
            } else {
//               usuario.setActivo(false);
                ((Stage) btnLogOutEquipo.getScene().getWindow()).close();
                FlowController.getInstance().delete("MantenimientoEquipos");
                FlowController.getInstance().delete("Principal");
            }
            FlowController.getInstance().delete("Principal");
            FlowController.getInstance().delete("Login");
            FlowController.getInstance().goViewInWindowModal("Login", getStage(), false);
        }
    }

    @FXML
    private void mantenimientoEquipo(ActionEvent event) {
        FlowController.getInstance().goView("MantenimientoEquipos");
    }

    @FXML
    private void informacionEquipo(ActionEvent event) {
    }

    @FXML
    private void mantenimientoCanchas(ActionEvent event) {
        FlowController.getInstance().goView("MantenimientoCancha");
    }

    @FXML
    private void informacionCanchas(ActionEvent event) {
        FlowController.getInstance().delete("Excel");
        FlowController.getInstance().goView("Excel");
    }

    @FXML
    private void buscarPartido(ActionEvent event) {
        FlowController.getInstance().goView("AgendaEquipo");
    }

    @FXML
    private void generarAgenda(ActionEvent event) {
        FlowController.getInstance().goView("AgendaAdministrador");
    }

}
