/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.util.BindingUtils;
import alquilercanchas.util.FlowController;
import alquilercanchas.util.Mensaje;
import alquilercanchas.model.Usuario1;
import alquilercanchas.service.UsuarioService;
import alquilercanchas.util.Respuesta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class RegistroController extends Controller implements Initializable {

    @FXML
    private JFXRadioButton rbCancha;
    @FXML
    private ToggleGroup TipoUsuario;
    @FXML
    private JFXRadioButton rbEquipo;
    @FXML
    private JFXTextField txtNombreUsuario;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private HBox hbClave;
    @FXML
    private JFXButton btnMostrarClave;
    @FXML
    private FontAwesomeIconView icon;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField txtPassword;
    @FXML
    private JFXButton btnMostrarClave1;
    @FXML
    private FontAwesomeIconView icon1;
    @FXML
    private JFXPasswordField passwordConf;
    @FXML
    private JFXTextField txtPasswordConf;
    @FXML
    private JFXButton btnRegresar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private HBox hbClaveConf;
    @FXML
    private JFXButton btnGuardar;

    //Atributos
    private LoginController login;
    private Usuario1 usuario;
    // private ArrayList<Usuario1> usuarios;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializaComponentes();

    }

    @Override
    public void initialize() {

    }

    /**
     * inicia los componentes necesarios para mostrar correctamente la pantalla
     */
    public void inicializaComponentes() {
        this.login = new LoginController();
        this.rbEquipo.setText("Equipo");
        this.rbEquipo.setUserData("E");
        this.rbCancha.setText("Cancha");
        this.rbCancha.setUserData("C");
        this.txtNombreUsuario.setPromptText("Capitán");
        this.txtNombreUsuario.setLabelFloat(true);
        this.txtUsuario.setPromptText("Usuario");
        this.txtUsuario.setLabelFloat(true);
        //Clave
        this.password.setPromptText("Clave");
        this.password.setLabelFloat(true);
        this.txtPassword.setPromptText("Clave");
        this.password.setLabelFloat(true);
        this.hbClave.getChildren().remove(this.txtPassword);
        //Confirmar Clave
        this.passwordConf.setPromptText("Confirmar Clave");
        this.passwordConf.setLabelFloat(true);
        this.txtPasswordConf.setPromptText("Confirmar Clave");
        this.txtPasswordConf.setLabelFloat(true);
        this.hbClaveConf.getChildren().remove(this.txtPasswordConf);
        this.btnGuardar.setText("Guardar");
        this.btnLimpiar.setText("Limpiar");
        this.btnRegresar.setText("Regresar");
        usuario = new Usuario1();
        //this.usuarios = (ArrayList<Usuario1>) AppContext.getInstance().get("usuarios");
        //usuario = (Usuario1) AppContext.getInstance().get("usuario");
        nuevo();
    }

    @FXML
    private void radioButon(ActionEvent event) {
        if (this.rbEquipo.isSelected()) {
            this.txtNombreUsuario.setPromptText("Capitán");
        } else {
            this.txtNombreUsuario.setPromptText("Administrador");
        }
    }

    public void nuevo() {
        unbind();
        this.passwordConf.setText("");
        usuario = new Usuario1();
        bind();
    }

    public void bind() {
        this.txtUsuario.textProperty().bindBidirectional(usuario.usuario);
        this.txtNombreUsuario.textProperty().bindBidirectional(usuario.nombre);
        this.password.textProperty().bindBidirectional(usuario.clave);
        BindingUtils.bindToggleGroupToProperty(TipoUsuario, usuario.tipoUsuario);
    }

    public void unbind() {
        this.txtUsuario.textProperty().unbindBidirectional(usuario.usuario);
        this.txtNombreUsuario.textProperty().unbindBidirectional(usuario.nombre);
        this.password.textProperty().unbindBidirectional(usuario.clave);
        BindingUtils.unbindToggleGroupToProperty(TipoUsuario, usuario.tipoUsuario);
    }

    /**
     * Verificacion de de clave
     */
    public Boolean confirmarClave() {
        return password.getText().endsWith(passwordConf.getText());
    }

    /**
     * Comprueva los campos necesarios
     *
     * @return
     */
    public String validarRequeridos() {
        String necesario = " ";
        Boolean valido = false;
        if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
            necesario += (" " + txtUsuario.getPromptText());
            valido = true;
        }
        if (txtNombreUsuario.getText() == null || txtNombreUsuario.getText().isEmpty()) {
            necesario += (" " + txtNombreUsuario.getPromptText());
            valido = true;
        }
        if (password.getText() == null || password.getText().isEmpty()) {
            necesario += (" " + password.getPromptText());
            valido = true;
        }
        if (passwordConf.getText() == null || passwordConf.getText().isEmpty()) {
            necesario += (" " + passwordConf.getPromptText());
            valido = true;
        }
        if (valido) {
            return necesario;
        } else {
            return " ";
        }

    }

    /**
     * Verificacion de usuario unico
     *
     * @return
     */
    public Boolean usuarioUnico() {
        UsuarioService user = new UsuarioService();
        return user.getUser(txtUsuario.getText());
    }

    /**
     * musra las claves
     *
     * @param event
     */
    @FXML
    private void muestraClave(ActionEvent event) {
        if (event.getSource().equals(this.btnMostrarClave)) {
            if (this.icon.getGlyphName().equalsIgnoreCase("EYE")) {
                this.hbClave.getChildren().add(this.txtPassword);
                this.txtPassword.setText(this.password.getText());
                this.hbClave.getChildren().remove(this.password);
                this.icon.setGlyphName("EYE_SLASH");
            } else {
                this.hbClave.getChildren().add(this.password);
                this.password.setText(this.txtPassword.getText());
                this.hbClave.getChildren().remove(this.txtPassword);
                this.icon.setGlyphName("EYE");
            }
        } else if (event.getSource().equals(this.btnMostrarClave1)) {
            if (this.icon1.getGlyphName().equalsIgnoreCase("EYE")) {
                this.hbClaveConf.getChildren().add(this.txtPasswordConf);
                this.txtPasswordConf.setText(this.passwordConf.getText());
                this.hbClaveConf.getChildren().remove(this.passwordConf);
                this.icon1.setGlyphName("EYE_SLASH");
            } else {
                this.hbClaveConf.getChildren().add(this.passwordConf);
                this.passwordConf.setText(this.txtPasswordConf.getText());
                this.hbClaveConf.getChildren().remove(this.txtPasswordConf);
                this.icon1.setGlyphName("EYE");
            }
        }
    }

    @FXML
    private void regresar(ActionEvent event) {

        FlowController.getInstance().delete("Registro");
        FlowController.getInstance().goViewInStage("Login", getStage());
        getStage().setWidth(505);
        getStage().setHeight(324);

    }

    @FXML
    private void limpiar(ActionEvent event) {
        nuevo();
    }

    @FXML
    private void guardar(ActionEvent event) {
        String necesarios = validarRequeridos();
        if (!necesarios.equals(" ")) {
            new Mensaje().show(Alert.AlertType.ERROR, "Campo requerido", "Es necesario llenar estos campos: " + "[ " + necesarios + "]");
        } else {
            if (!usuarioUnico()) {
                if (confirmarClave()) {
//                    nuevo();
                    UsuarioService usuarioService = new UsuarioService();
                    Respuesta resp = usuarioService.guardarUsuario(usuario);
                    if (resp.getEstado()) {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardado", getStage(), "¡Usuario guardado exitosamente!");

                    } else {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Error al guardar", getStage(), resp.getMensaje());

                    }
                } else {
                    new Mensaje().show(Alert.AlertType.ERROR, "Clave", "Las claves no coinciden");
                }
            } else {
                new Mensaje().show(Alert.AlertType.ERROR, "Usuario", "El usuario ingresado ya existe, por favor introdusca otro.");
            }
        }
    }

}
