/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Cancha;
import alquilercanchas.model.Partido;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class AgendaEquipoController extends Controller implements Initializable {

    @FXML
    private JFXComboBox<Cancha> cbCancha;
    @FXML
    private JFXDatePicker dpFecha;
    @FXML
    private JFXButton btnFiltrar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private FlowPane fpAgendaEquipos;
    //Variables
    private ObservableList<Partido> partidos;
    private ObservableList<Cancha> canchas;
    private Usuario1 usuario;

    private Boolean boolCancha;
    private Boolean boolFecha;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        iniciarComponentes();
        cargarPartidos();
    }

    /**
     * inicia los cmponentes
     */
    public void iniciarComponentes() {
        fpAgendaEquipos.getChildren().clear();
        btnFiltrar.setText("Filtrar");
        btnLimpiar.setText("Limpiar");
        cbCancha.getSelectionModel().select(-1);
        dpFecha.setValue(null);
        //variables
        usuario = new Usuario1();
        partidos = FXCollections.observableArrayList();
        canchas = FXCollections.observableArrayList();
        cargarVariables();
        cbCancha.setItems(canchas);
        boolCancha = Boolean.FALSE;
        boolFecha = Boolean.FALSE;
    }

    /**
     * Carga las variables principales
     */
    public void cargarVariables() {
        usuario = (Usuario1) AppContext.getInstance().get("usuario");
        partidos = (ObservableList<Partido>) AppContext.getInstance().get("partidos");
        canchas = (ObservableList<Cancha>) AppContext.getInstance().get("obCancha");
    }

    /**
     * Carga todos los partidos
     */
    public void cargarPartidos() {
        fpAgendaEquipos.getChildren().clear();
        partidos.stream().filter(habilitadosPredicate).forEach((t) -> {
            fpAgendaEquipos.getChildren().add(t);
        });
    }

    @FXML
    private void cargarCancha(ActionEvent event) {
        boolCancha = Boolean.TRUE;
    }

    @FXML
    private void cargarFecha(ActionEvent event) {
        boolFecha = Boolean.TRUE;
    }


    /**
     * Aplica los diferentes fintros
     *
     * @param event
     */
    @FXML
    private void filtrar(ActionEvent event) {
        if (!boolCancha && !boolFecha) {
            new Mensaje().showModal(Alert.AlertType.WARNING, " ", getStage(), "Debe seleccionar algun filtro");

        }
        if (boolCancha && !boolFecha) {
            fpAgendaEquipos.getChildren().clear();
            partidos.stream().filter(habilitadosPredicate.and(canchasPredicate)).forEach((t) -> {
                fpAgendaEquipos.getChildren().add(t);
            });

        }
        if (!boolCancha && boolFecha) {
            fpAgendaEquipos.getChildren().clear();
            partidos.stream().filter(habilitadosPredicate.and(diaPredicate)).forEach((t) -> {
                fpAgendaEquipos.getChildren().add(t);
            });
        }
        if (boolCancha && boolFecha) {
            fpAgendaEquipos.getChildren().clear();
           partidos.stream().filter(habilitadosPredicate.and(canchasPredicate).and(diaPredicate)).forEach((t) -> {
               fpAgendaEquipos.getChildren().add(t);
           });
        }
    }

    /**
     * Limpia los espacios de los filtros
     *
     * @param event
     */
    @FXML
    private void limpiar(ActionEvent event) {
        cbCancha.getSelectionModel().select(-1);
        boolCancha = Boolean.FALSE;
        dpFecha.getEditor().clear();
        boolFecha = Boolean.FALSE;
        cargarPartidos();

    }

    Predicate<Partido> habilitadosPredicate = (Partido t) -> t.getEstado().equals(true) && t.getFinalizado().equals(false);

    Predicate<Partido> canchasPredicate = (Partido t) -> t.getCancha().equals(cbCancha.getValue());

    Predicate<Partido> diaPredicate = (Partido t) -> t.getDia().equals(dpFecha.getValue());

}//fin
