/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Contacto;
import alquilercanchas.model.Equipo;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Formato;
import alquilercanchas.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class MantenimientoEquiposController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtUsuario;
    
    private JFXTextField txtContrasena;
    @FXML
    private JFXTextField txtNombreEquipo;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnLimpiar;
    private JFXButton btnBorrar;
    @FXML
    private JFXTextField txtNombreContacto;
    @FXML
    private JFXTextField txtNumeroCotacto;
    @FXML
    private JFXButton btnAgregarCon;
    @FXML
    private TableView<Contacto> tvContactos;
    @FXML
    private TableColumn<Contacto, SimpleStringProperty> tcNombre;
    @FXML
    private TableColumn<Contacto, SimpleStringProperty> tcTelefono;
    @FXML
    private HBox hbClave;
    @FXML
    private JFXButton btnMostrarClave;
    @FXML
    private FontAwesomeIconView icon;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField txtPassword;
    @FXML
    private JFXButton btnAgregarCambiarBandera;

    //Atribustos
    private Equipo equipo;

    private ObservableList<Equipo> equipos;

    private Usuario1 usuario;

    private Contacto contacto;

    private Contacto eliminarContacto;

    private Boolean primera = false;

    private ImageView ivBandera;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iniciarComponentes();

    }

    /**
     * inicia los componentes necesarioa asi como la carga inicial de datos,
     * eventos.
     */
    public void iniciarComponentes() {
        //Datos
        txtNombre.setPromptText("Nombre");
        txtNombre.setLabelFloat(true);

        txtUsuario.setPromptText("Usuario");
        txtUsuario.setLabelFloat(true);

        password.setPromptText("Clave");
        password.setLabelFloat(true);

        txtPassword.setPromptText("Clave");
        txtPassword.setLabelFloat(true);

        txtNombreEquipo.setPromptText("Nombre del Equipo");
        txtNombreEquipo.setLabelFloat(true);

        txtNombreContacto.setPromptText("Nombre de Contacto");
        txtNombreContacto.setLabelFloat(true);

        txtNumeroCotacto.setPromptText("Numero de telefono");
        txtNumeroCotacto.setLabelFloat(true);
        this.hbClave.getChildren().remove(this.txtPassword);
        //botones 
        btnLimpiar.setText("Eliminar Contacto");
        btnGuardar.setText("Guardar");
        btnAgregarCon.setText("Agregar");
        btnAgregarCambiarBandera.setText("Agregar Bandera");
        //Formato
        txtNumeroCotacto.textFormatterProperty().set(Formato.getInstance().integerFormat());
        //inicializacion de los objetos
        equipo = new Equipo();
        usuario = new Usuario1();
        //extraer datos importantes
        //listas
        this.equipos = (ObservableList<Equipo>) AppContext.getInstance().get("equipos");
        this.ivBandera = (ImageView) AppContext.getInstance().get("Bandera");
        //Tabla
        tcNombre.setText("Nombre");
        tcTelefono.setText("Telefono");
        tcNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tcTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        tvContactos.setItems(equipo.getIntegrantes());
        //evento a table view
        tvContactos.setOnMouseClicked((MouseEvent MouseEvent) -> {
            eliminarContacto = getSeleccionada();
        });

        //cargado de datos
        cargarDatosUsuario();
        cargarEvento();

    }

    @Override
    public void initialize() {
        recargarDatos();
    }

    /**
     * recarga los datos cuando se inicia sesion con un usario que ya tiene los
     * datos completos
     */
    public void recargarDatos() {
        if (usuario.getEquipo() != null) {
            cargarDatosUsuario();
            if (usuario.getEquipo().getBandera() != null) {
                ivBandera.setImage(usuario.getEquipo().getBandera());
                btnAgregarCambiarBandera.setText("Cambiar Bandera");
            }
            txtNombreEquipo.textProperty().bindBidirectional(usuario.getEquipo().nombre);
            tvContactos.setItems(usuario.getEquipo().getIntegrantes());
        }else{
             equipo.setCapitan(usuario);
             usuario.setEquipo(equipo);
        }
    }

    /**
     * carga los datos de los usarios
     */
    public void cargarDatosUsuario() {
        unbind();
        usuario = (Usuario1) AppContext.getInstance().get("usuario");
        bind();
    }

    public void bind() {
        txtNombre.textProperty().bindBidirectional(usuario.nombre);
        txtUsuario.textProperty().bindBidirectional(usuario.usuario);
        password.textProperty().bindBidirectional(usuario.clave);
        txtNombreEquipo.textProperty().bindBidirectional(equipo.nombre);

    }

    public void unbind() {
        txtNombre.textProperty().unbindBidirectional(usuario.nombre);
        txtUsuario.textProperty().unbindBidirectional(usuario.usuario);
        password.textProperty().unbindBidirectional(usuario.clave);
        txtNombreEquipo.textProperty().unbindBidirectional(equipo.nombre);

    }

    @FXML
    private void guardar(ActionEvent event) {
        if (equipo.getNombre() != null) {
            if (usuario.getEquipo().getIntegrantes().size() > 1) {
                usuario.setDatosCompletos(1);
                usuario.getEquipo().setBandera(ivBandera.getImage());
                usuario.getEquipo().setBoton(new JFXButton("Mostrar"));
                equipos.add(usuario.getEquipo());
                new Mensaje().show(Alert.AlertType.INFORMATION, "Guardado", "Guardado exitosamente");
            } else {
                new Mensaje().show(Alert.AlertType.ERROR, "Contactos", "Es necesario que se ingresen mas contactos");
            }
        } else {
            new Mensaje().show(Alert.AlertType.ERROR, "Nombre equipo", "Debe completar el nombre del equipo primero");
        }
    }

    @FXML
    private void limpiar(ActionEvent event) {
        usuario.getEquipo().getIntegrantes().remove(eliminarContacto);
       
    }


    @FXML
    private void agregarContacto(ActionEvent event) {
        if (camposRequeridosContacto()) {
            contacto = new Contacto(txtNombreContacto.getText(), txtNumeroCotacto.getText());
            usuario.getEquipo().getIntegrantes().add(contacto);
            //limpiar campos
            txtNombreContacto.setText("");
            txtNumeroCotacto.setText("");
        } else {
            new Mensaje().show(Alert.AlertType.ERROR, "Campo requerido", "Es necesario llenar los campos de los datos del contacto");
        }
    }

    public Boolean camposRequeridosContacto() {
        if (txtNombreContacto.getText() == null || txtNumeroCotacto.getText().isEmpty()) {
            return false;
        }
        if (txtNumeroCotacto.getText() == null || txtNumeroCotacto.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Carga el evento de buscar bandera a el boton agregar
     */
    public void cargarEvento() {
        btnAgregarCambiarBandera.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");
            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );
            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(getStage());

            // Mostar la imagen
            if (imgFile != null) {
                Image image = new Image("file:" + imgFile.getAbsolutePath());
                ivBandera.setImage(image);
                 btnAgregarCambiarBandera.setText("Cambiar Bandera");
            }
        });
    }

      @FXML
    private void muestraClave(ActionEvent event) {
        if (this.icon.getGlyphName().equalsIgnoreCase("EYE")) {
            this.hbClave.getChildren().add(this.txtPassword);
            this.txtPassword.setText(this.password.getText());
            txtPassword.textProperty().bindBidirectional(usuario.clave);
            this.hbClave.getChildren().remove(this.password);
            this.icon.setGlyphName("EYE_SLASH");
        } else {
            this.hbClave.getChildren().add(this.password);
            this.password.setText(this.txtPassword.getText());
            password.textProperty().bindBidirectional(usuario.clave);
            this.hbClave.getChildren().remove(this.txtPassword);
            this.icon.setGlyphName("EYE");
        }
    }

    /**
     * seleccion de la lista de la table view
     *
     * @return retorna el elemento selecionado
     */
    public Contacto getSeleccionada() {
        if (tvContactos != null) {
            List<Contacto> tabla = tvContactos.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final Contacto competicionSeleccionada = tabla.get(0);
                return competicionSeleccionada;
            }
        }
        return null;
    }
}
