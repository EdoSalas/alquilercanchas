/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Equipo;
import alquilercanchas.model.Partido;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Funciones;
import alquilercanchas.util.TablaInfo;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class InfoEquipoController extends Controller implements Initializable {

    @FXML
    private ImageView imgEquipo;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtPuntaje;
    @FXML
    private JFXTextField txtRendimiento;
    @FXML
    private TableView<TablaInfo> tvInfo;
    @FXML
    private TableColumn<TablaInfo, String> clCancha;
    @FXML
    private TableColumn<TablaInfo, String> clEquipo;
    @FXML
    private TableColumn<TablaInfo, String> clGoles;
    @FXML
    private TableColumn<TablaInfo, String> clGolesContrincante;
    @FXML
    private TableColumn<TablaInfo, String> clContrincante;
    @FXML
    private TableColumn<TablaInfo, String> clPuntosObtenidos;

    private ObservableList<Partido> partidos;
    private ObservableList<Equipo> equipos;
    private ObservableList<TablaInfo> tabla;
    private Equipo equipo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }

    @Override
    public void initialize() {

    }

    public void inicializa() {
        this.partidos = (ObservableList<Partido>) AppContext.getInstance().get("partidos");
        this.equipo = (Equipo) AppContext.getInstance().get("equipoInfo");
        this.equipos = (ObservableList<Equipo>) AppContext.getInstance().get("equipos");
        this.tabla = FXCollections.observableArrayList();

        this.txtNombre.setText(equipo.getNombre().toString());
        this.txtPuntaje.setText(equipo.getPuntos().toString());
        this.txtRendimiento.setText(equipo.getRendimiento().toString());
        this.imgEquipo.setImage(equipo.getBandera());
        //Tabla
        this.clCancha.setText("Cancha");
        this.clEquipo.setText("Local");
        this.clGoles.setText("Goles a Favor");
        this.clGolesContrincante.setText("Goles en Contra");
        this.clContrincante.setText("Contrincante");
        this.clPuntosObtenidos.setText("Puntos Obtenidos");

        this.clCancha.setCellValueFactory(new PropertyValueFactory<>("cancha"));
        this.clEquipo.setCellValueFactory(new PropertyValueFactory<>("equipo"));
        this.clGoles.setCellValueFactory(new PropertyValueFactory<>("goles"));
        this.clGolesContrincante.setCellValueFactory(new PropertyValueFactory<>("golesContrincante"));
        this.clContrincante.setCellValueFactory(new PropertyValueFactory<>("contrincante"));
        this.clPuntosObtenidos.setCellValueFactory(new PropertyValueFactory<>("puntaje"));
        this.tvInfo.setItems(this.tabla);
        llenaTabla();
    }

    public String ganador(Partido partido) {
        if (partido.getGanador().equals(null)) {
            return "1";
        } else if (partido.getGanador().equals(equipo)) {
            return "3";
        } else if (!partido.getGanador().equals(equipo)) {
            return "0";
        } else {
            return "0";
        }
    }

    public void llenaTabla() {
        try {
            Predicate<Partido> equipoJuega = (Partido t) -> t.getEsta(equipo);
            this.partidos.stream().filter(equipoJuega).forEach((z) -> {
                tabla.add(new TablaInfo(z.getCancha().getNombre(), z.getLocal(equipo).getNombre().toString(), z.getLocal(equipo).getGoles().toString(),
                        z.getContrincante(equipo).getGoles().toString(), z.getContrincante(equipo).getNombre().toString(), ganador(z)));
            });
        } catch (Exception e) {
            Funciones.mensajeError("Error al cargar datos");
        }
    }
}
