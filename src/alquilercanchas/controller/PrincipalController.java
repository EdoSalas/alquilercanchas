/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Usuario1;
import alquilercanchas.util.AppContext;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class PrincipalController extends Controller implements Initializable {

    @FXML
    private JFXHamburger hamMenu;
    @FXML
    private JFXDrawer dwMenu;
    @FXML
    private VBox vbPrincipal;
    @FXML
    private ImageView ivBandera;

    private Usuario1 usuario;
    @FXML
    private Label lblUsuario;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicalizarComponentes();
        AppContext.getInstance().set("centro", vbPrincipal);
    }

    @Override
    public void initialize() {
    }

    /**
     * carga datos iniciales y sus componentes
     */
    public void inicalizarComponentes() {
        hamMenu();
        this.usuario = (Usuario1) AppContext.getInstance().get("usuario");
        this.lblUsuario.setText(this.usuario.getNombre());
        AppContext.getInstance().set("Bandera", ivBandera);
        cargarImagen();
    }

    public void hamMenu() {
        try {
            HBox hb = FXMLLoader.load(getClass().getResource("/alquilercanchas/view/Menu.fxml"));
            this.dwMenu.setSidePane(hb);
        } catch (IOException ex) {
            Logger.getLogger(PrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
        HamburgerSlideCloseTransition ham = new HamburgerSlideCloseTransition(hamMenu);
        ham.setRate(-1);
        this.hamMenu.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
            ham.setRate(ham.getRate() * -1);
            ham.play();
            if (this.dwMenu.isShown()) {
                this.dwMenu.close();
            } else {
                this.dwMenu.open();
            }
        });
    }

    public void cargarImagen() {
        if (usuario.getEquipo() != null) {
            if (usuario.getEquipo().getBandera() != null) {
                ivBandera.setImage(usuario.getEquipo().getBandera());
            } else {
                ivBandera.setImage(new Image("alquilercanchas/resources/8353.png"));
            }
        } else {
            ivBandera.setImage(new Image("alquilercanchas/resources/8353.png"));
        }

    }
}
