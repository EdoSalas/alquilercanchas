/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Cancha;
import alquilercanchas.model.Equipo;
import alquilercanchas.model.Partido;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.FlowController;
import alquilercanchas.util.Mensaje;
import alquilercanchas.model.Usuario1;
import alquilercanchas.service.UsuarioService;
import alquilercanchas.util.Respuesta;
import alquilercanchas.util.TablaExcel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class LoginController extends Controller implements Initializable {

    @FXML
    private ImageView imgLogo;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXButton btnMostrarClave;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField txtPassword;
    @FXML
    private JFXButton btnIniciar;
    @FXML
    private JFXButton btnRegistro;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private HBox hbClave;
    @FXML
    private FontAwesomeIconView icon;

    //Atributos 
    private ArrayList<Usuario1> usuarios;
    private Usuario1 usuario;
    private ObservableList<Equipo> equipos = FXCollections.observableArrayList();
    private ObservableList<Cancha> canchas = FXCollections.observableArrayList();
    private ObservableList<Partido> partidos = FXCollections.observableArrayList();
    private ObservableList<Partido> auxPartido = FXCollections.observableArrayList();
    private ObservableList<TablaExcel> tabla = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializarComponentes();

    }

    @Override
    public void initialize() {

    }

    /**
     * Inicializa los componentes
     */
    public void inicializarComponentes() {
        this.btnIniciar.setText("Iniciar");
        this.btnLimpiar.setText("Limpiar");
        this.btnRegistro.setText("Registrarse");
        this.hbClave.getChildren().remove(this.txtPassword);
        this.txtUsuario.setPromptText("Usuario");
        this.txtUsuario.setLabelFloat(true);
        this.password.setPromptText("Clave");
        this.password.setLabelFloat(true);
        this.txtPassword.setPromptText("Clave");
        this.txtPassword.setLabelFloat(true);
        this.usuario = new Usuario1();
        this.usuarios = new ArrayList();

        //Datos al app contex list
        AppContext.getInstance().set("usuarios", this.usuarios);
        AppContext.getInstance().set("equipos", equipos);
        AppContext.getInstance().set("canchas", canchas);
        AppContext.getInstance().set("usuario", usuario);
        AppContext.getInstance().set("partidos", partidos);
        AppContext.getInstance().set("auxPartido", this.auxPartido);
        AppContext.getInstance().set("obTabla", this.tabla);
        //usuariosTesteo();

    }

    @FXML
    private void muestraClave(ActionEvent event) {
        if (this.icon.getGlyphName().equalsIgnoreCase("EYE")) {
            this.hbClave.getChildren().add(this.txtPassword);
            this.txtPassword.setText(this.password.getText());
            this.hbClave.getChildren().remove(this.password);
            this.icon.setGlyphName("EYE_SLASH");
        } else {
            this.hbClave.getChildren().add(this.password);
            this.password.setText(this.txtPassword.getText());
            this.hbClave.getChildren().remove(this.txtPassword);
            this.icon.setGlyphName("EYE");
        }
    }

    @FXML
    private void iniciar(ActionEvent event) {
        if (iniciarSesion()) {
            ((Stage) btnIniciar.getScene().getWindow()).close();
            FlowController.getInstance().goMain();
        }
    }

    /**
     * Verifica que el usuario y la clave sean correctos
     *
     * @return
     */
    public Boolean iniciarSesion() {
        Boolean x=false;
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnLimpiar.getScene().getWindow(), "Debe ingresar un usuario.");
            } else if (password.getText() == null || password.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnLimpiar.getScene().getWindow(), "Debe ingresar una contraseña");
            } else {
                UsuarioService usuarioService = new UsuarioService();
                Respuesta respuesta = usuarioService.getUsuario(txtUsuario.getText(), password.getText());
                if (respuesta.getEstado()) {
                    AppContext.getInstance().set("Usuario", (Usuario1) respuesta.getResultado("Usuario"));
                    System.out.println(((Usuario1) respuesta.getResultado("Usuario")).tipoUsuario);
                    System.out.println("Validacion correcta");
                    x=true;
                } else {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(), respuesta.getMensaje());
                    x=false;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
        return x;
    }    
    /**
     * Veerfica que los campos no esten vacios
     *
     * @return
     */
    public Boolean camposRequeridos() {
        if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciar.getScene().getWindow(), "Es necesario digitar un usuario para ingresar.");
            return false;
        } else if (password.getText() == null || password.getText().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnIniciar.getScene().getWindow(), "Es necesario digitar la clave para ingresar.");
            return false;
        } else {
            return true;
        }
    }

    @FXML
    private void registro(ActionEvent event) {
        FlowController.getInstance().goViewInStage("Registro", getStage());
        getStage().setWidth(622);
        getStage().setHeight(384);
    }

    @FXML
    private void limpiar(ActionEvent event) { 
        this.txtUsuario.setText("");
        this.password.setText("");
        this.txtPassword.setText("");
    }

    public HBox getClave() {
        return this.hbClave;
    }

    /*
    public void usuariosTesteo() {
        //Testing
        Equipo a = new Equipo("equipo1", 0, 0.0f, new JFXButton("Mostrar"));
        Usuario1 b = new Usuario1("Equipo", "Test Cli", "cli", "cli", false);
        b.setEquipo(a);
        this.usuarios.add(new Usuario1("Cancha", "Test", "admin", "admin", false));
        this.usuarios.add(new Usuario1("Equipo", "Test Eq", "cliente", "cliente", false));
        this.usuarios.add(b);
        this.usuarios.add(new Usuario1("Cancha", "Test Ad", "ad", "ad", false));

        this.equipos.add(a);
        this.equipos.add(new Equipo("equipo2", 3, 100.0f, new JFXButton("Mostrar")));
        this.equipos.add(new Equipo("equipo3", 0, 0.0f, new JFXButton("Mostrar")));
        this.equipos.add(new Equipo("equipo4", 0, 0.0f, new JFXButton("Mostrar")));
    }
     */
}
