/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Equipo;
import alquilercanchas.model.Partido;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.InfoWindowOptions;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class ModificadorAdministradorController extends Controller implements Initializable, MapComponentInitializedListener {

    @FXML
    private ImageView imgEquipo1;
    @FXML
    private ImageView imgEquipo2;
    @FXML
    private JFXButton btnHabilitar;
    @FXML
    private HBox hbEquipo1;
    @FXML
    private JFXButton btnMenos1;
    @FXML
    private HBox hbEquipo2;
    @FXML
    private JFXButton btnMenos2;
    @FXML
    private HBox hbBotones;
    @FXML
    private JFXButton btnInhabilitar;
    @FXML
    private JFXComboBox<Equipo> cbEquipo1;
    @FXML
    private JFXComboBox<Equipo> cbEquipo2;
    @FXML
    private GoogleMapView mapa;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXTextField txtGolesEqui1;
    @FXML
    private JFXTextField txtGolesEqui2;
    @FXML
    private JFXCheckBox chxFinalizado;

    private ObservableList<Partido> obPartido;
    private ObservableList<Partido> partidos;
    private ObservableList<Equipo> equipos;
    private GoogleMap map;
    private Partido partido;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iniciarComponentes();
        verificarCargados();
    }

    @Override
    public void initialize() {
        iniciarComponentes();
        verificarCargados();
    }

    public void iniciarComponentes() {

        this.mapa.addMapInializedListener(this);
        partidos = (ObservableList<Partido>) AppContext.getInstance().get("partidos");
        partido = (Partido) AppContext.getInstance().get("modificar");
        btnGuardar.setText("Guardar");
        btnHabilitar.setText("Habilitar");
        btnInhabilitar.setText("Inhabilitar");
        chxFinalizado.setText("Partido Finalizado");
        hbBotones.getChildren().clear();
        if (partido.getEstado()) {
            hbBotones.getChildren().add(btnInhabilitar);
        } else {
            hbBotones.getChildren().add(btnHabilitar);
        }
        equipos = FXCollections.observableArrayList();
        chxFinalizado.setSelected(partido.getFinalizado());
        chxFinalizado.setVisible(partido.getFinalizado());
        iniciarVariables();
        comboBox();
    }

    /**
     * Muestra los equipos en el combo box
     */
    public void comboBox() {
        cbEquipo1.setItems(equipos);
        cbEquipo1.setConverter(new StringConverter<Equipo>() {
            @Override
            public String toString(Equipo object) {
                return object.getNombre();
            }

            @Override
            public Equipo fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        cbEquipo2.setItems(equipos);
        cbEquipo2.setConverter(new StringConverter<Equipo>() {
            @Override
            public String toString(Equipo object) {
                return object.getNombre();
            }

            @Override
            public Equipo fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    /**
     * Inicia las variables globales
     */
    public void iniciarVariables() {

        equipos = (ObservableList<Equipo>) AppContext.getInstance().get("equipos");
    }

    /**
     * Habilita o inavilita el partido
     *
     * @param event
     */
    @FXML
    private void habilitar(ActionEvent event) {
        if (!chxFinalizado.isSelected()) {

            hbBotones.getChildren().clear();
            ImageView imgIna = new ImageView(new Image("alquilercanchas/resources/1028.png"));
            ImageView imgAvi = new ImageView(new Image("alquilercanchas/resources/8353.png"));
            imgIna.setFitHeight(50);
            imgIna.setFitWidth(50);
            imgAvi.setFitHeight(50);
            imgAvi.setFitWidth(50);
            if (!partido.getEstado()) {
                if (partido.getEquipos().size() == 1) {
                    partido.setText(partido.getCancha() + "\nFecha: " + partido.getDia() + "\nHora: " + partido.getHora() + "\nEsperando Reto");
                } else if (partido.getEquipos().size() == 2) {
                    partido.setText(partido.getCancha() + "\nFecha: " + partido.getDia() + "\nHora: " + partido.getHora() + "\nOcupada");
                } else {
                    partido.setText(partido.getCancha() + "\nFecha: " + partido.getDia() + "\nHora: " + partido.getHora() + "\nDisponible");
                }
                partido.setGraphic(imgAvi);
                partido.setEstado(Boolean.TRUE);
                hbBotones.getChildren().add(btnInhabilitar);
            } else {
                partido.setText("INHABILITADO\n" + partido.getHora() + "\n" + partido.getDia());
                partido.setGraphic(imgIna);
                partido.setEstado(Boolean.FALSE);
                hbBotones.getChildren().add(btnHabilitar);
            }
        } else {
            new Mensaje().show(Alert.AlertType.WARNING, "Accion invalida", "El partido ha finalizado, no se permiten realizar cambios");
        }
    }

    @Override
    public void mapInitialized() {
        LatLong latLong = new LatLong(Double.parseDouble(partido.getCancha().getLat().toString()), Double.parseDouble(partido.getCancha().getLon().toString()));
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(latLong)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);
        this.map = this.mapa.createMap(mapOptions);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLong);
        Marker marker = new Marker(markerOptions);
        this.map.addMarker(marker);

        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h2>" + partido.getCancha().getTitulo() + "</h2>" + partido.getCancha().getDireccion() + "<br>");
        InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
        infoWindow.open(this.map, marker);
    }

    /**
     * Verifica si el partido ya cuenta con equipos y los carga.
     */
    public void verificarCargados() {
        if (!partido.getEquipos().isEmpty()) {
            if (partido.getEquipos().size() == 2) {
                imgEquipo1.setImage(partido.getEquipos().get(0).getBandera());
                cbEquipo1.setValue(partido.getEquipos().get(0));
//                if (partido.getMarcador().getGoles(partido.getEquipos().get(0)) != null) {
//                    txtGolesEqui1.setText((partido.getMarcador().getGoles(partido.getEquipos().get(0))).toString());
//                }
                imgEquipo2.setImage(partido.getEquipos().get(1).getBandera());
                cbEquipo2.setValue(partido.getEquipos().get(1));
//                if (partido.getMarcador().getGoles(partido.getEquipos().get(1)) != null) {
//                    txtGolesEqui1.setText((partido.getMarcador().getGoles(partido.getEquipos().get(1))).toString());
//                }
            } else {
                imgEquipo1.setImage(partido.getEquipos().get(0).getBandera());
                cbEquipo1.setValue(partido.getEquipos().get(0));
            }
        }
    }

    @FXML
    private void EliminarEquipo1(ActionEvent event) {
        if (!chxFinalizado.isSelected()) {
            if (cbEquipo1.getValue() != null) {
                if (new Mensaje().showConfirmation("Eliminar equipo", getStage(), "Seguro que desea eliminar el equipo")) {
                    cbEquipo1.getSelectionModel().clearSelection();
                    txtGolesEqui1.setText("");
                    imgEquipo1.setImage(null);
                }
            } else {
                new Mensaje().show(Alert.AlertType.INFORMATION, "", "Nada que eliminar");
            }
        } else {
            new Mensaje().show(Alert.AlertType.WARNING, "Accion invalida", "El partido ha finalizado, no se permiten realizar cambios");
        }
    }

    @FXML
    private void EliminarEquipo2(ActionEvent event) {
        if (!chxFinalizado.isSelected()) {
            if (cbEquipo2.getValue() != null) {
                if (new Mensaje().showConfirmation("Eliminar equipo", getStage(), "Seguro que desea eliminar el equipo")) {
                    cbEquipo2.getSelectionModel().clearSelection();
                    txtGolesEqui2.setText("");
                    imgEquipo2.setImage(null);
                }
            } else {
                new Mensaje().show(Alert.AlertType.INFORMATION, "", "Nada que eliminar");
            }
        } else {
            new Mensaje().show(Alert.AlertType.WARNING, "Accion invalida", "El partido ha finalizado, no se permiten realizar cambios");
        }
    }

    /**
     * Guarda los cambios en la partida
     *
     * @param event
     */
    @FXML
    private void GuardarPartidos(ActionEvent event) {
        if (!chxFinalizado.isSelected()) {
            partido.getEquipos().clear();
            if (cbEquipo1.getValue() != null) {
                imgEquipo1.setImage(cbEquipo1.getValue().getBandera());
                partido.getEquipos().add(cbEquipo1.getValue());
            }
            if (cbEquipo2.getValue() != null) {
                imgEquipo2.setImage(cbEquipo2.getValue().getBandera());
                partido.getEquipos().add(cbEquipo2.getValue());
            }
            asignarMarcador();
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "GUARDADO", getStage(), "Cambios guardados con exito");
        } else {
            new Mensaje().show(Alert.AlertType.WARNING, "Accion invalida", "El partido ha finalizado, no se permiten realizar cambios");
        }
        habilitarFinalizar();
        if (partido.getEquipos().size() == 1) {
            partido.setText(partido.getCancha() + "\nFecha: " + partido.getDia() + "\nHora: " + partido.getHora() + "\nEsperando Reto");
        } else if (partido.getEquipos().size() == 2) {
            partido.setText(partido.getCancha() + "\nFecha: " + partido.getDia() + "\nHora: " + partido.getHora() + "\nOcupada");
        } else {
            partido.setText(partido.getCancha() + "\nFecha: " + partido.getDia() + "\nHora: " + partido.getHora() + "\nDisponible");
        }
    }

    public void asignarMarcador() {
        if (cbEquipo1.getValue() != null && cbEquipo2.getValue() != null && !txtGolesEqui1.getText().isEmpty() && !txtGolesEqui1.getText().isEmpty()) {
//            partido.getMarcador().setEquipo1(cbEquipo1.getValue(), Integer.parseInt(txtGolesEqui1.getText()));
//            partido.getMarcador().setEquipo2(cbEquipo2.getValue(), Integer.parseInt(txtGolesEqui2.getText()));
        }

    }

    @FXML
    private void finalizarPartido(ActionEvent event) {
        if (chxFinalizado.isSelected()) {
            if (!txtGolesEqui1.getText().equals(txtGolesEqui2.getText())) {
                partido.setFinalizado(Boolean.TRUE);
                if (partido.getGanador() == null && !partido.getEmpate() && partido.getEquipos().size() == 2) {
//                    if (partido.getMarcador().getGoles(cbEquipo1.getValue()) > partido.getMarcador().getGoles(cbEquipo2.getValue())) {
//                        partido.setGanador(cbEquipo1.getValue());
//                        cbEquipo1.getValue().setPuntos(cbEquipo1.getValue().getPuntos() + 3);
//                    } else {
//                        if (partido.getMarcador().getGoles(cbEquipo1.getValue()) < partido.getMarcador().getGoles(cbEquipo2.getValue())) {
//                            partido.setGanador(cbEquipo2.getValue());
//                            cbEquipo2.getValue().setPuntos(cbEquipo2.getValue().getPuntos() + 3);
//                        } else {
//                            partido.setEmpate(Boolean.TRUE);
//                            cbEquipo1.getValue().setPuntos(cbEquipo1.getValue().getPuntos() + 1);
//                            cbEquipo2.getValue().setPuntos(cbEquipo2.getValue().getPuntos() + 1);
//                        }
//                    }
                }
                this.obPartido = (ObservableList<Partido>) AppContext.getInstance().get("obPartido");
                this.obPartido.add(this.partido);
                actualizarRanking(partido);
            }

        } else {
            partido.setFinalizado(Boolean.FALSE);
        }

    }

    public void habilitarFinalizar() {
        if (!txtGolesEqui1.getText().isEmpty() && !txtGolesEqui2.getText().isEmpty()) {
            chxFinalizado.setVisible(true);
        }
    }

    public void actualizarRanking(Partido partido) {

    }

}//Fin clase
