/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Cancha;
import alquilercanchas.model.Partido;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Exportar;
import alquilercanchas.util.Funciones;
import alquilercanchas.util.TablaExcel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class ExcelController extends Controller implements Initializable {

    @FXML
    private JFXComboBox<Cancha> cbCanchas;
    @FXML
    private JFXDatePicker dateInicio;
    @FXML
    private JFXDatePicker dateCorte;
    @FXML
    private JFXButton btnFiltrar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private JFXButton btnExcel;
    @FXML
    private TableView<TablaExcel> tbExcel;
    @FXML
    private TableColumn<TablaExcel, String> clNombre;
    @FXML
    private TableColumn<TablaExcel, String> clFechaIni;
    @FXML
    private TableColumn<TablaExcel, String> clFechaLimite;
    @FXML
    private TableColumn<TablaExcel, String> clGanancias;
    @FXML
    private TableColumn<TablaExcel, String> clOcupados;
    @FXML
    private TableColumn<TablaExcel, String> clVacios;

    private ObservableList<Cancha> canchas;
    private ObservableList<Partido> obPartido;
    private ObservableList<Partido> auxPartido;
    private ObservableList<Partido> partidos;
    private ObservableList<TablaExcel> tabla;
    private Exportar exporta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }

    @Override
    public void initialize() {
        cargaCombo();
    }

    public void inicializa() {
        this.exporta = new Exportar();
        this.btnFiltrar.setText("Filtrar");
        this.btnExcel.setText("Exportar");
        this.btnLimpiar.setText("Limpiar");
        cargaCombo();
        this.tabla = (ObservableList<TablaExcel>) AppContext.getInstance().get("obTabla");

        this.clNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.clFechaIni.setCellValueFactory(new PropertyValueFactory<>("fechaIni"));
        this.clFechaLimite.setCellValueFactory(new PropertyValueFactory<>("fechaCorte"));
        this.clGanancias.setCellValueFactory(new PropertyValueFactory<>("ganancias"));
        this.clOcupados.setCellValueFactory(new PropertyValueFactory<>("jugados"));
        this.clVacios.setCellValueFactory(new PropertyValueFactory<>("noJugados"));
        this.tbExcel.setItems(this.tabla);
    }

    @FXML
    private void filtrar(ActionEvent event) {
        this.obPartido = (ObservableList<Partido>) AppContext.getInstance().get("obPartido");
        this.auxPartido = (ObservableList<Partido>) AppContext.getInstance().get("auxPartido");
        try {
//            this.auxPartido.addAll(this.partidos.stream().filter(x -> x.getCancha().equals(this.cbCanchas.getSelectionModel().getSelectedItem())
//                && (x.getDia().isEqual(this.dateInicio.getValue())
//                || x.getDia().isAfter(this.dateInicio.getValue())
//                || x.getDia().isBefore(this.dateCorte.getValue())
//                || x.getDia().isEqual(this.dateCorte.getValue()))).collect(Collectors.toList()));
        llenaTabla();
        } catch (Exception e) {
            Funciones.mensajeError("No existen datos para filtrar");
        }
    }

    @FXML
    private void limpiar(ActionEvent event) {
        this.cbCanchas.getSelectionModel().clearSelection();
        this.dateInicio.setValue(null);
        this.dateCorte.setValue(null);
    }

    @FXML
    private void generarExcel(ActionEvent event) {
        this.exporta.exporta(this.tabla);
        limpiar(event);
    }

    public Double monto() {
        Double monto = 0.0;
//        List<Partido> collect = this.obPartido.stream().filter(x -> x.getCancha().equals(this.cbCanchas.getSelectionModel().getSelectedItem())
//                && (x.getDia().isEqual(this.dateInicio.getValue())
//                || x.getDia().isAfter(this.dateInicio.getValue())
//                || x.getDia().isBefore(this.dateCorte.getValue())
//                || x.getDia().isEqual(this.dateCorte.getValue()))).collect(Collectors.toList());

//        monto += collect.stream().filter(x -> x.getHora().getHour() < 16 && x.getEstado().equals(true)).count() * collect.get(0).getCancha().getPrecioDia();
//        monto += collect.stream().filter(x -> x.getHora().getHour() > 16 && x.getEstado().equals(true)).count() * collect.get(0).getCancha().getPrecioNoche();
        return monto;
    }

    public Integer jugados() {
        this.partidos = (ObservableList<Partido>) AppContext.getInstance().get("partidos");
//        List<Partido> collect = this.partidos.stream().filter(x -> x.getCancha().equals(this.cbCanchas.getSelectionModel().getSelectedItem())
//                && (x.getDia().isEqual(this.dateInicio.getValue())
//                || x.getDia().isAfter(this.dateInicio.getValue())
//                || x.getDia().isBefore(this.dateCorte.getValue())
//                || x.getDia().isEqual(this.dateCorte.getValue()))).collect(Collectors.toList());

//        return (int) (long) collect.stream().filter(x -> x.getEstado().equals(true)).count();
        return null;
    }

    public Integer noJugados() {
        this.partidos = (ObservableList<Partido>) AppContext.getInstance().get("partidos");
//        List<Partido> collect = this.partidos.stream().filter(x -> x.getCancha().equals(this.cbCanchas.getSelectionModel().getSelectedItem())
//                && (x.getDia().isEqual(this.dateInicio.getValue())
//                || x.getDia().isAfter(this.dateInicio.getValue())
//                || x.getDia().isBefore(this.dateCorte.getValue())
//                || x.getDia().isEqual(this.dateCorte.getValue()))).collect(Collectors.toList());
//        return (int) (long) collect.stream().filter(x -> x.getEstado().equals(false)).count();
        return null;
    }

    public void cargaCombo() {
        this.canchas = (ObservableList<Cancha>) AppContext.getInstance().get("obCancha");
        this.cbCanchas.setItems((ObservableList<Cancha>) AppContext.getInstance().get("obCancha"));
        cbCanchas.setConverter(new StringConverter<Cancha>() {
            @Override
            public String toString(Cancha object) {
                return object.getNombre();
            }

            @Override
            public Cancha fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    public void llenaTabla() {
        this.tabla.add(new TablaExcel(this.auxPartido.get(auxPartido.size() - 1).getCancha().getNombre(), this.dateInicio.getValue().toString(), this.dateCorte.getValue().toString(), monto().toString(), jugados().toString(), noJugados().toString()));
        this.tbExcel.setItems(this.tabla);
        this.tbExcel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

}
