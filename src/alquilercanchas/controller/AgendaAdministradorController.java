/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Cancha;
import alquilercanchas.model.Partido;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.FlowController;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import cr.ac.una.canchasws.controller.LocalDate;
import cr.ac.una.canchasws.controller.LocalTime;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author Eduardo
 */
public class AgendaAdministradorController extends Controller implements Initializable {

    @FXML
    private JFXDatePicker dpDia;
    @FXML
    private FlowPane fpCalendario;
    @FXML
    private JFXComboBox<Cancha> cbCanchas;
    private Cancha cancha;
    private Usuario1 usuario;
    private Integer tiempoApertua;
    private Integer cantPartidos;
    private ObservableList<Partido> partidos;
    private LocalTime horario;
    private LocalDate dia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    @Override
    public void initialize() {
        iniciarComponentes();
        cargarVariables();

    }

    EventHandler<MouseEvent> seleccion = new EventHandler<MouseEvent>() {
        public void handle(MouseEvent event) {
            Partido a;
            Usuario1 user;
            a = (Partido) event.getSource();
            user = (Usuario1) AppContext.getInstance().get("usuario");
            AppContext.getInstance().set("modificar", a);

            if (user.getTipoUsuario().equals("Equipo")) {
                FlowController.getInstance().delete("ModificadorEquipo");
                FlowController.getInstance().goView("ModificadorEquipo");
            } else {
                FlowController.getInstance().delete("ModificadorAdministrador");
                FlowController.getInstance().goView("ModificadorAdministrador");
            }
        }
    };

    /**
     * inicia los componentes
     */
    public void iniciarComponentes() {
        dpDia.setPromptText("DIA");
        dpDia.setEditable(false);

        //Variables
        usuario = new Usuario1();
        partidos = FXCollections.observableArrayList();
        cancha = new Cancha();
        tiempoApertua = 0;
        cargarVariables();
        cargarBox();
    }

    /**
     * carga las cariables principales
     */
    public void cargarVariables() {
        usuario = (Usuario1) AppContext.getInstance().get("usuario");
        partidos = (ObservableList<Partido>) AppContext.getInstance().get("partidos");
    }

    /**
     * indica la cantidad de partidos posibles en la cacha
     *
     * @param cancha
     */
    public Integer cantidadPartidos(Cancha cancha) {
//        tiempoApertua = Math.abs(((((cancha.getAbierto().getHour()) * 60) + cancha.getAbierto().getMinute()) - ((cancha.getCerrado().getHour() * 60) + cancha.getCerrado().getMinute())) / 60);
        return tiempoApertua / 2;
    }

    public void crearAgenda(LocalDate dia) {
//        horario = cancha.getAbierto().;
        for (int i = 0; i < cantidadPartidos(cancha); i++) {
            ImageView imgIna = new ImageView(new Image("alquilercanchas/resources/1028.png"));
            ImageView imgAvi = new ImageView(new Image("alquilercanchas/resources/8353.png"));
            imgIna.setFitHeight(50);
            imgIna.setFitWidth(50);
            imgAvi.setFitHeight(50);
            imgAvi.setFitWidth(50);
            Partido a = new Partido();
            a.setOnMouseClicked(seleccion);
            a.setHora(horario);
            a.setDia(dia);
            a.setCancha(cancha);
            a.setGraphic(imgAvi);
            a.setMaxSize(200, 80);
            a.setMinSize(200, 80);
            a.setEstado(Boolean.TRUE);
            a.setFinalizado(Boolean.FALSE);
            a.setText(a.getCancha() + "\nFecha: " + a.getDia() + "\nHora: " + a.getHora() + "\nDisponible");
            fpCalendario.getChildren().add(a);
            partidos.add(a);
//            horario = horario.plusMinutes(120);
        }
    }

    public void cargarBox() {
        //carga lista al combo box
        cbCanchas.setItems(usuario.getCanchas());
        cbCanchas.setOnAction((ActionEvent x) -> {
            cancha = cbCanchas.getValue();
            dpDia.getEditor().clear();
            dpDia.getEditor().deselect();
            fpCalendario.getChildren().clear();

        });
        dpDia.setOnAction((ActionEvent x) -> {
//            dia = dpDia.getValue();
            cancha = cbCanchas.getValue();
            cargarAgenda(dia);
        });
    }

    public void cargarAgenda(LocalDate fecha) {
        fpCalendario.getChildren().clear();
        if (!partidos.stream().filter(x -> x.getCancha().equals(cancha) && x.getDia().equals(fecha)).collect(Collectors.toList()).isEmpty()) {
            fpCalendario.getChildren().addAll(partidos.stream().filter(x -> x.getCancha().equals(cancha) && x.getDia().equals(fecha)).collect(Collectors.toList()));
        } else {
            crearAgenda(fecha);
        }
    }

}//Fin clase
