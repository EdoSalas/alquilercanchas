/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Partido;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Mensaje;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.InfoWindowOptions;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author liedu
 */
public class ModificadorEquipoController extends Controller implements Initializable, MapComponentInitializedListener {

    @FXML
    private ImageView imgEquipo1;
    @FXML
    private JFXTextField txtEquipo1;
    @FXML
    private ImageView imgEquipo2;
    private JFXTextField txtEquipo2;
    @FXML
    private GoogleMapView mapa;
    @FXML
    private JFXButton btnRegistrarse;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private Label lbInformacion;
    @FXML
    private JFXTextField txtequipo2;

    private Partido partido;
    private Usuario1 usuario;
    private ObservableList<Partido> obPartido;
    private GoogleMap map;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initialize() {
        iniciarComponentes();
        verificarCargados();
        habilitarBotones();
    }

    @Override
    public void mapInitialized() {
        LatLong latLong = new LatLong(Double.parseDouble(partido.getCancha().getLat().toString()), Double.parseDouble(partido.getCancha().getLon().toString()));
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(latLong)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);
        this.map = this.mapa.createMap(mapOptions);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLong);
        Marker marker = new Marker(markerOptions);
        this.map.addMarker(marker);

        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h2>" + partido.getCancha().getTitulo() + "</h2>" + partido.getCancha().getDireccion() + "<br>");
        InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
        infoWindow.open(this.map, marker);
    }

    /**
     * inicia los componentes
     */
    public void iniciarComponentes() {
        this.mapa.addMapInializedListener(this);
        btnRegistrarse.setText("Registrarse");
        btnCancelar.setText("Cancelar Registro");
        iniciarVariables();
        lbInformacion.setText("CANCHA: " + partido.getCancha().getNombre() + "\t\tDIA: "
                + partido.getDia().toString() + "\t\tHORA: " + partido.getHora().toString());
    }

    /**
     * Inicia las Variables globales
     */
    public void iniciarVariables() {
        usuario = (Usuario1) AppContext.getInstance().get("usuario");
        partido = (Partido) AppContext.getInstance().get("modificar");
    }

    public void verificarCargados() {
//        if (!partido.getEquipos().isEmpty()) {
//            if (partido.getEquipos().size() == 2) {
//                if (partido.getMarcador().getGoles(partido.getEquipos().get(0)) != null) {
//                    imgEquipo1.setImage(partido.getEquipos().get(0).getBandera());
//                    txtEquipo1.setText(partido.getEquipos().get(0).getNombre());
//                }
//                if (partido.getMarcador().getGoles(partido.getEquipos().get(1)) != null) {
//                    imgEquipo2.setImage(partido.getEquipos().get(1).getBandera());
//                    txtEquipo2.setText(partido.getEquipos().get(1).getNombre());
//                }
//            } else {
//                imgEquipo1.setImage(partido.getEquipos().get(0).getBandera());
//                txtEquipo1.setText(partido.getEquipos().get(0).getNombre());
//            }
//        }
    }

    public void habilitarBotones() {
        if (!partido.getEquipos().isEmpty() && partido.getEquipos().size() == 2) {
            if (usuario.getEquipo().equals(partido.getEquipos().get(0)) || usuario.getEquipo().equals(partido.getEquipos().get(1))) {
                btnRegistrarse.setDisable(true);
                btnCancelar.setDisable(false);
            } else {
                btnRegistrarse.setDisable(true);
                btnCancelar.setDisable(true);
            }
        }
    }

    @FXML
    private void registrarse(ActionEvent event) {
        try {
                this.partido.getEquipos().add(usuario.getEquipo());
        if (txtEquipo1.getText().isEmpty()) {
            imgEquipo1.setImage(usuario.getEquipo().getBandera());
            txtEquipo1.setText(usuario.getEquipo().getNombre());
        }else{
            imgEquipo2.setImage(usuario.getEquipo().getBandera());
            txtEquipo2.setText(usuario.getEquipo().getNombre());
        }
        habilitarBotones();
        } catch (Exception e) {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Error Inesperado", getStage(), "Se a presentado un error inesperado,\npor favor realice la prueba desde el administrador de equipos");
        }
    
    }

    @FXML
    private void cancelar(ActionEvent event) {
        if (new Mensaje().showConfirmation("Eliminar equipo", getStage(), "Seguro que desea eliminar el equipo")) {
            if (txtEquipo1.getText().equals(usuario.getEquipo().getNombre())) {
                partido.getEquipos().remove(usuario.getEquipo());
                txtEquipo1.clear();
                imgEquipo1.setImage(null);
            }
            if (txtEquipo2.getText().equals(usuario.getEquipo().getNombre())) {
                partido.getEquipos().remove(usuario.getEquipo());
                txtEquipo2.clear();
                imgEquipo2.setImage(null);
            }
        }

    }

}//fin clase
