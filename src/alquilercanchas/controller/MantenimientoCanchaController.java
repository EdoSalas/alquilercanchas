/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Cancha;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.AppContext;
import alquilercanchas.util.Mensaje;
import com.jfoenix.controls.JFXPasswordField;
import alquilercanchas.util.FlowController;
import alquilercanchas.util.Formato;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class MantenimientoCanchaController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNombreUs;
    @FXML
    private JFXTextField txtUs;
    @FXML
    private JFXComboBox<Cancha> cbCanchas;
    @FXML
    private JFXTextField txtNombreCancha;
    @FXML
    private JFXTextField txtTelefono;
    @FXML
    private JFXTimePicker dtHorarioApertura;
    @FXML
    private JFXTimePicker dtHorarioClausura;
    @FXML
    private JFXTextField txtCostoNoche;
    @FXML
    private JFXTextField txtCostoDia;
    @FXML
    private JFXTextField txtCantidadJugadores;
    @FXML
    private JFXTextField txtCoorLatitud;
    @FXML
    private JFXTextField txtCoorLongitud;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnLimpiar;
    @FXML
    private JFXButton btnBorrar;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXButton btnMapa;
    @FXML
    private HBox hbClave;
    @FXML
    private JFXButton btnMostrarClave;
    @FXML
    private FontAwesomeIconView icon;
    @FXML
    private JFXTextField txtPassword;
    @FXML
    private JFXTextField txtTitulo;
    @FXML
    private JFXTextField txtDireccion;

    //Atributos
    private ArrayList<JFXTextField> camposTxt = new ArrayList();

    private ArrayList<JFXTimePicker> camposTp = new ArrayList();

    private ObservableList<Cancha> obCancha = FXCollections.observableArrayList();

    private Usuario1 usuario;

    private Cancha cancha;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializarComponentes();
        //indica los campos requeridos
        indicarRequeridos();
    }

    @Override
    public void initialize() {

    }

    /**
     * inicia los componentes y carga los datos iniciales
     */
    public void inicializarComponentes(){
        this.txtNombreUs.setPromptText("Nombre");
        this.txtNombreUs.setLabelFloat(true);
        this.txtUs.setPromptText("Usuario");
        this.txtUs.setLabelFloat(true);
        this.password.setPromptText("Clave");
        this.password.setLabelFloat(true);
        this.txtPassword.setPromptText("Clave");
        this.txtPassword.setLabelFloat(true);
        this.txtNombreCancha.setPromptText("Nombre Cancha");
        this.txtNombreCancha.setLabelFloat(true);
        this.txtTelefono.setPromptText("Telefono");
        this.txtTelefono.setLabelFloat(true);
        this.dtHorarioApertura.setPromptText("Abierto");
        this.dtHorarioApertura.setEditable(false);
        this.dtHorarioClausura.setPromptText("Cerrado");
        this.dtHorarioClausura.setEditable(false);
        this.txtCostoDia.setPromptText("Costo Dia");
        this.txtCostoDia.setLabelFloat(true);
        this.txtCostoNoche.setPromptText("Costo Noche");
        this.txtCostoNoche.setLabelFloat(true);
        this.txtCantidadJugadores.setPromptText("Cantidad Jugadores");
        this.txtCantidadJugadores.setLabelFloat(true);
        this.txtCoorLatitud.setPromptText("Latitud");
        this.txtCoorLatitud.setLabelFloat(true);
        this.txtCoorLongitud.setPromptText("Longitud");
        this.txtCoorLatitud.setLabelFloat(true);
        this.txtTitulo.setPromptText("Titulo");
        this.txtTitulo.setLabelFloat(true);
        this.txtDireccion.setPromptText("Direccion");
        this.txtDireccion.setLabelFloat(true);
        this.hbClave.getChildren().remove(this.txtPassword);

        this.txtCoorLatitud.setEditable(false);
        this.txtCoorLongitud.setEditable(false);

        //Formato
        this.txtTelefono.textFormatterProperty().set(Formato.getInstance().integerFormat());
        this.txtTelefono.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(8));
        this.txtCostoDia.textFormatterProperty().set(Formato.getInstance().integerFormat());
        this.txtCostoDia.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(5));
        this.txtCostoNoche.textFormatterProperty().set(Formato.getInstance().integerFormat());
        this.txtCostoNoche.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(5));
        this.txtTitulo.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(100));
        this.txtDireccion.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(100));
        this.txtCantidadJugadores.textFormatterProperty().set(Formato.getInstance().integerFormat());
        this.txtCantidadJugadores.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(2));
        this.txtNombreCancha.textFormatterProperty().set(Formato.getInstance().maxLengthFormat(40));

        //objetos
        this.usuario = new Usuario1();
        this.cancha = new Cancha();
        //botones  
        this.btnBorrar.setText("Borrar");
        this.btnGuardar.setText("Guardar Cancha");
        this.btnLimpiar.setText("Nueva");
        //cargar datos
        cargarDatosUsuario();
        //cargar conexiones cancha
        nuevaCancha();
        this.cbCanchas.setItems(this.usuario.getCanchas());
        cargarBox();

        if (this.obCancha.size() == 0) {
            canchasProp();
        }
    }

    /**
     * carga el objeto elegido en el combo box
     */
    public void cargarBox() {
        //carga lista al combo box
        this.cbCanchas.setOnAction((ActionEvent x) -> {
            unbindCacha();
            this.cancha = this.cbCanchas.getValue();
            //crea un nuevo objeto si el actual es borrado
            if (this.cancha != null) {
                bindCancha();
            } else {
                this.cancha = new Cancha();
                nuevaCancha();
            };
        });
    }

    /**
     * carga los datos de los usuarios
     */
    public void cargarDatosUsuario() {
        unbindUsuario();
        this.usuario = (Usuario1) AppContext.getInstance().get("usuario");
        bindUsuario();
    }

    public void bindUsuario() {
        this.txtNombreUs.textProperty().bindBidirectional(this.usuario.nombre);
        this.txtUs.textProperty().bindBidirectional(this.usuario.usuario);
        this.password.textProperty().bindBidirectional(this.usuario.clave);

    }

    public void unbindUsuario() {
        this.txtNombreUs.textProperty().unbindBidirectional(this.usuario.nombre);
        this.txtUs.textProperty().unbindBidirectional(this.usuario.usuario);
        this.password.textProperty().unbindBidirectional(this.usuario.clave);
    }

    /**
     * crear conexiones para una nueva cancha
     *
     * @param nueva
     */
    public void nuevaCancha() {
        unbindCacha();
        this.cancha = new Cancha();
        bindCancha();
        this.dtHorarioApertura.getEditor().clear();
        this.dtHorarioClausura.getEditor().clear();
    }

    public void bindCancha() {
        this.txtNombreCancha.textProperty().bindBidirectional(this.cancha.nombre);
        this.txtTelefono.textProperty().bindBidirectional(this.cancha.numeroTelefono);
        this.txtCantidadJugadores.textProperty().bindBidirectional(this.cancha.numeroJugadores);
        this.txtCostoDia.textProperty().bindBidirectional(this.cancha.precioDia);
        this.txtCostoNoche.textProperty().bindBidirectional(this.cancha.precioNoche);
        this.txtTitulo.textProperty().bindBidirectional(this.cancha.titulo);
        this.txtDireccion.textProperty().bindBidirectional(this.cancha.direccion);
        this.txtCoorLatitud.textProperty().bindBidirectional(this.cancha.lat);
        this.txtCoorLongitud.textProperty().bindBidirectional(this.cancha.lon);
//        this.dtHorarioApertura.valueProperty().bindBidirectional(this.cancha.abierto);
//        this.dtHorarioClausura.valueProperty().bindBidirectional(this.cancha.cerrado);
    }

    public void unbindCacha() {
        this.txtNombreCancha.textProperty().unbindBidirectional(this.cancha.nombre);
        this.txtTelefono.textProperty().unbindBidirectional(this.cancha.numeroTelefono);
        this.txtCantidadJugadores.textProperty().unbindBidirectional(this.cancha.numeroJugadores);
        this.txtCostoDia.textProperty().unbindBidirectional(this.cancha.precioDia);
        this.txtCostoNoche.textProperty().unbindBidirectional(this.cancha.precioNoche);
        this.txtTitulo.textProperty().unbindBidirectional(this.cancha.titulo);
        this.txtDireccion.textProperty().unbindBidirectional(this.cancha.direccion);
        this.txtCoorLatitud.textProperty().unbindBidirectional(this.cancha.lat);
        this.txtCoorLongitud.textProperty().unbindBidirectional(this.cancha.lon);
//        this.dtHorarioApertura.valueProperty().unbindBidirectional(this.cancha.abierto);
//        this.dtHorarioClausura.valueProperty().unbindBidirectional(this.cancha.cerrado);
    }

    /**
     * veridica que los campos esten llenos
     *
     * @return retorna los nombres de los campos que aun estan vacios y son
     * necesarios
     */
    public String camposRequeridos() {

        Boolean validos = true;
        String invalidos = "";

        for (JFXTextField node : this.camposTxt) {
            if (node.equals("") || node.getText() == null) {
                if (validos) {
                    invalidos += (node.getPromptText());
                } else {
                    invalidos += ("," + node.getPromptText());
                }
                validos = false;
            }
        }
        if (this.txtDireccion.equals("") || this.txtDireccion.getText() == null) {
            if (validos) {
                invalidos += (this.txtDireccion.getPromptText());
            } else {
                invalidos += ("," + this.txtDireccion.getPromptText());
            }
            validos = false;
        }
        for (JFXTimePicker node : this.camposTp) {

            if (node.getValue() == null) {
                if (validos) {
                    invalidos += (node.getPromptText());
                } else {
                    invalidos += ("," + node.getPromptText());
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }

    }

    /**
     * carga los campos requeridos para la cancha
     */
    public void indicarRequeridos() {
        this.camposTxt.clear();
        this.camposTxt.addAll(Arrays.asList(this.txtCoorLatitud, this.txtCoorLongitud, this.txtCostoDia,
                this.txtCostoNoche, this.txtCantidadJugadores, this.txtNombreCancha, this.txtTelefono));
        this.camposTp.clear();
        this.camposTp.addAll(Arrays.asList(this.dtHorarioApertura, this.dtHorarioClausura));
    }

    @FXML
    private void guardar(ActionEvent event) {
        String campos = camposRequeridos();
        if (!campos.isEmpty()) {
            new Mensaje().show(Alert.AlertType.ERROR, "Guardar Cancha", campos);
        } else {
            if (this.usuario.getCanchas().size() > 0) {
                if (!this.usuario.getCanchas().stream().anyMatch(x -> x.getNombre().endsWith(this.txtNombreCancha.getText()))) {
                    this.usuario.getCanchas().add(this.cancha);
                    this.obCancha.add(this.cancha);
                    nuevaCancha();
                    new Mensaje().show(Alert.AlertType.INFORMATION, "Guardado", "Se a guardado correctamente");
                } else {
                    new Mensaje().show(Alert.AlertType.INFORMATION, "Actualizado", "La informacion a sido actualizada correctamente");
                }
            } else {
                this.usuario.getCanchas().add(this.cancha);
                this.obCancha.add(this.cancha);
                nuevaCancha();
                new Mensaje().show(Alert.AlertType.INFORMATION, "Guardado", "Se a guardado correctamente");
            }
        }
    }

    /**
     * Hace la funcion de nueva cancha
     *
     * @param event
     */
    @FXML
    private void limpiar(ActionEvent event) {
        nuevaCancha();
    }

    @FXML
    private void borrar(ActionEvent event) {
        if (this.cancha != null) {
            this.usuario.getCanchas().remove(this.cancha);
            nuevaCancha();
        }
    }

    @FXML
    private void mostrarMapa(ActionEvent event) {
        AppContext.getInstance().set("mantenimientoCancha", this);
        FlowController.getInstance().delete("Mapa");
        FlowController.getInstance().goView("Mapa");
    }

    @FXML
    private void muestraClave(ActionEvent event) {
        if (this.icon.getGlyphName().equalsIgnoreCase("EYE")) {
            this.hbClave.getChildren().add(this.txtPassword);
            this.txtPassword.setText(this.password.getText());
            this.txtPassword.textProperty().bindBidirectional(this.usuario.clave);
            this.hbClave.getChildren().remove(this.password);
            this.icon.setGlyphName("EYE_SLASH");
        } else {
            this.hbClave.getChildren().add(this.password);
            this.password.setText(this.txtPassword.getText());
            this.password.textProperty().bindBidirectional(this.usuario.clave);
            this.hbClave.getChildren().remove(this.txtPassword);
            this.icon.setGlyphName("EYE");
        }
    }

    public JFXTextField getTxtCoorLatitud() {
        return txtCoorLatitud;
    }

    public JFXTextField getTxtCoorLongitud() {
        return txtCoorLongitud;
    }

    public JFXTextField getTxtTitulo() {
        return txtTitulo;
    }

    public JFXTextField getTxtDireccion() {
        return txtDireccion;
    }

    /**
     * usuarios de prueba
     */
    public void canchasProp(){
        this.usuario = (Usuario1) AppContext.getInstance().get("usuario");
        if (this.usuario.getUsuario().equalsIgnoreCase("admin") && this.usuario.getCanchas().isEmpty()) {
//            this.obCancha.add(new Cancha("Prueba 1", LocalTime.now(), LocalTime.now().plusHours(6), 15000, 14000, 8, "87654321", "Cancha 1", "Rivas", "9.417495739937605", "-83.65923029049043"));
//            this.obCancha.add(new Cancha("Prueba 2", LocalTime.now(), LocalTime.now().plusHours(10), 14000, 13000, 7, "76543210", "Cancha 2", "Rivas", "9.417495739937605", "-83.65923029049043"));
//            this.obCancha.add(new Cancha("Prueba 3", LocalTime.now(), LocalTime.now().plusHours(12), 13000, 15000, 9, "65432109", "Cancha 3", "Rivas", "9.417495739937605", "-83.65923029049043"));
            this.usuario.getCanchas().add(this.obCancha.get(0));
            this.usuario.getCanchas().add(this.obCancha.get(1));
            this.usuario.getCanchas().add(this.obCancha.get(2));
        }

        AppContext.getInstance().set("obCancha", this.obCancha);

        
    }

}
