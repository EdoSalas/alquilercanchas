/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.util.AppContext;
import alquilercanchas.util.FlowController;
import alquilercanchas.util.Funciones;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import netscape.javascript.JSObject;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class MapaController extends Controller implements Initializable, MapComponentInitializedListener {

    @FXML
    private JFXButton btnAtras;
    @FXML
    private GoogleMapView mapa;
    @FXML
    private JFXTextField txtLatitud;
    @FXML
    private JFXTextField txtLongitud;
    @FXML
    private JFXButton btnMarcadores;
    @FXML
    private JFXTextField txtTitulo;
    @FXML
    private JFXTextField txtDescripcion;
    @FXML
    private JFXButton btnIr;

    private GoogleMap map;
    private Double latitud;
    private Double longitud;
    private LatLong latlong;
    private ArrayList<Marker> markers;
    private MantenimientoCanchaController mcc;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.mapa.addMapInializedListener(this);
        this.latitud = 0.0;
        this.longitud = 0.0;
        this.btnIr.setText("Ir");
        this.txtLatitud.setPromptText("Latitud");
        this.txtLatitud.setLabelFloat(true);
        this.txtLongitud.setPromptText("Longitud");
        this.txtLongitud.setLabelFloat(true);
        this.txtTitulo.setPromptText("Titulo");
        this.txtTitulo.setLabelFloat(true);
        this.txtDescripcion.setPromptText("Descripcion");
        this.txtDescripcion.setLabelFloat(true);
        this.btnMarcadores.setText("Marcador");
        this.markers = new ArrayList();

    }

    @Override
    public void initialize() {
        
    }

    @Override
    public void mapInitialized() {
        LatLong latLong = new LatLong(9.372615671204043, -83.70214563472871);
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(latLong)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);
        this.map = this.mapa.createMap(mapOptions);
        
        actualiza();
    }

    public void centraMapa() {
        Double lat = Double.parseDouble(this.mcc.getTxtCoorLatitud().getText());
        Double lon = Double.parseDouble(this.mcc.getTxtCoorLongitud().getText());
        LatLong latLong = new LatLong(lat, lon);
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(latLong)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);
        this.map = this.mapa.createMap(mapOptions);
    }

    @FXML
    private void atras(ActionEvent event) {
        limpiar();
        if (!this.markers.isEmpty()) {
            this.map.removeMarker(this.markers.get(0));
        }
        this.markers.clear();
        FlowController.getInstance().goView("MantenimientoCancha");
    }

    @FXML
    private void coordenadas(MouseEvent e) {
        this.map.addUIEventHandler(UIEventType.click, (JSObject jso) -> {
            LatLong latLong = new LatLong((JSObject) (jso).getMember("latLng"));
            txtLatitud.setText(Double.toString(latLong.getLatitude()));
            txtLongitud.setText(Double.toString(latLong.getLongitude()));
        });
    }

    @FXML
    private void agregaMarcador() {
        if (!this.txtLatitud.getText().trim().isEmpty() && !this.txtLongitud.getText().trim().isEmpty()) {
            this.latitud = Double.parseDouble(this.txtLatitud.getText());
            this.longitud = Double.parseDouble(this.txtLongitud.getText());
            this.latlong = new LatLong(this.latitud, this.longitud);
        }
        this.mcc = (MantenimientoCanchaController) AppContext.getInstance().get("mantenimientoCancha");
        //Guarda la información en la pantalla de mantenimiento
        this.mcc.getTxtCoorLatitud().setText(this.latitud.toString());
        this.mcc.getTxtCoorLongitud().setText(this.longitud.toString());
        this.mcc.getTxtTitulo().setText(this.txtTitulo.getText());
        this.mcc.getTxtDireccion().setText(this.txtDescripcion.getText());
        centraMapa();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(this.latlong);
        Marker marker = new Marker(markerOptions);
        if (!this.markers.isEmpty()) {
            this.map.removeMarker(this.markers.get(0));
            this.markers.remove(this.markers.get(0));
        }
        this.markers.add(marker);
        this.map.addMarker(marker);

        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h2>" + this.txtTitulo.getText() + "</h2>" + this.txtDescripcion.getText() + "<br>");
        InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
        infoWindow.open(this.map, marker);

        limpiar();
    }

    @FXML
    private void soloNumeros(KeyEvent e) {
        if (!Funciones.isNumber(e.getCharacter())) {
            e.consume();
        }
    }

    @FXML
    private void ir(ActionEvent event) {
        LatLong latLong = new LatLong(Double.parseDouble(this.txtLatitud.getText()), Double.parseDouble(this.txtLongitud.getText()));
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(latLong)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);
        this.map = this.mapa.createMap(mapOptions);
    }

    public void limpiar(){
        this.txtLatitud.setText("");
        this.txtLongitud.setText("");
        this.txtTitulo.setText("");
        this.txtDescripcion.setText("");
    }
    
    public void actualiza(){
        this.mcc = (MantenimientoCanchaController) AppContext.getInstance().get("mantenimientoCancha");
        if (!this.mcc.getTxtCoorLatitud().getText().trim().isEmpty()) {
            this.txtLatitud.setText(this.mcc.getTxtCoorLatitud().getText());
            this.txtLongitud.setText(this.mcc.getTxtCoorLongitud().getText());
            this.txtTitulo.setText(this.mcc.getTxtTitulo().getText());
            this.txtDescripcion.setText(this.mcc.getTxtDireccion().getText());
            this.latitud = Double.parseDouble(this.txtLatitud.getText());
            this.longitud = Double.parseDouble(this.txtLongitud.getText());
            this.latlong = new LatLong(this.latitud, this.longitud);
            agregaMarcador();
        }
    }
}
