/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.controller;

import alquilercanchas.model.Equipo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Matami
 */
public class HistorialEquipoController extends Controller implements Initializable {

    @FXML
    private Label lbNombreEquipo;
    @FXML
    private TableColumn<?, ?> tcContrincante;
    @FXML
    private TableColumn<?, ?> tcMarcador;
    @FXML
    private TableColumn<?, ?> tcCancha;
    @FXML
    private TableColumn<?, ?> tcFechaHora;
    @FXML
    private TableView<Equipo> tvHistorial;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializarComponentes();
    }    
    
    public void inicializarComponentes(){
        tcContrincante.setText("Contrincante");
        tcCancha.setText("Cancha");
        tcFechaHora.setText("Fecha y Hora");
        tcMarcador.setText("Marcador");
    }
    @Override
    public void initialize() {
    }
    
}
