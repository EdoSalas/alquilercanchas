/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.service;

import alquilercanchas.model.Partido;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.Respuesta;
import cr.ac.una.canchasws.controller.CanchaDto;
import cr.ac.una.canchasws.controller.PartidoDto;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matami
 */
public class PartidoService {

    public Respuesta getPartidoIdCan1(Long idCan) {
        try {

            if (idCan == null || idCan <= 0) {
                return new Respuesta(false, "Favor ingrear los parametros de busqueda", "");
            }
            List<PartidoDto> parDto = getPartidoIdCan(idCan);
            List<Partido> partidos = new ArrayList();
            for (PartidoDto p : parDto) {
                Partido par = new Partido(p);
                partidos.add(par);
            }

            return new Respuesta(true, "", "", "Partidos", partidos);

        } catch (Exception ex) {
            Logger.getLogger(PartidoService.class.getName()).log(Level.SEVERE, "Error al obtener lista de partidos", ex);
            return new Respuesta(false, "Ocurrio un error durante la consulta", "");

        }
    }

    public Respuesta getPartidoIdMarc1(Long idMarc) {
        try {

            if (idMarc == null || idMarc <= 0) {
                return new Respuesta(false, "Favor ingrear los parametros de busqueda", "");
            }
            PartidoDto par = getPartidoIdMarc(idMarc);

            return new Respuesta(true, "", "", "canchas", new Partido(par));

        } catch (Exception ex) {
            Logger.getLogger(PartidoService.class.getName()).log(Level.SEVERE, "Error al obtener lista de partidos", ex);
            return new Respuesta(false, "Ocurrio un error durante la consulta", "");

        }
    }

    private static java.util.List<cr.ac.una.canchasws.controller.PartidoDto> getPartidoIdCan(java.lang.Long idCan) {
        cr.ac.una.canchasws.controller.PartidoController_Service service = new cr.ac.una.canchasws.controller.PartidoController_Service();
        cr.ac.una.canchasws.controller.PartidoController port = service.getPartidoControllerPort();
        return port.getPartidoIdCan(idCan);
    }

    private static PartidoDto getPartidoIdMarc(java.lang.Long idMarc) {
        cr.ac.una.canchasws.controller.PartidoController_Service service = new cr.ac.una.canchasws.controller.PartidoController_Service();
        cr.ac.una.canchasws.controller.PartidoController port = service.getPartidoControllerPort();
        return port.getPartidoIdMarc(idMarc);
    }

}
