/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.service;

import alquilercanchas.model.Marcador;
import alquilercanchas.model.Partido;
import alquilercanchas.util.Respuesta;
import cr.ac.una.canchasws.controller.MarcadorDto;
import cr.ac.una.canchasws.controller.PartidoDto;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matami
 */
public class MarcadorService {

    /**
     * Consulta la lita de carcadores de un partido especifico con sus
     * respectivos equipos
     *
     * @param IdPar
     * @return
     */
    public Respuesta getMarcadorIdPar1(Long IdPar) {
        try {

            if (IdPar == null || IdPar <= 0) {
                return new Respuesta(false, "Favor ingrear los parametros de busqueda", "");
            }
            List<MarcadorDto> marDto = getMarcadorIdPar(IdPar);
            List<Marcador> marcador = new ArrayList();
            for (MarcadorDto m : marDto) {
                Marcador mar = new Marcador(m);
                marcador.add(mar);
            }

            return new Respuesta(true, "", "", "Marcador", marcador);

        } catch (Exception ex) {
            Logger.getLogger(PartidoService.class.getName()).log(Level.SEVERE, "Error al obtener lista de marcadores", ex);
            return new Respuesta(false, "Ocurrio un error durante la consulta", "");

        }
    }

    public Respuesta getMarcadorIdEq(Long dEq) {
        try {

            if (dEq == null || dEq <= 0) {
                return new Respuesta(false, "Favor ingrear los parametros de busqueda", "");
            }
            List<MarcadorDto> marDto = getMarcadorIdEq_1(dEq);
            List<Marcador> marcador = new ArrayList();
            for (MarcadorDto m : marDto) {
                Marcador mar = new Marcador(m);
                marcador.add(mar);
            }

            return new Respuesta(true, "", "", "Marcador", marcador);

        } catch (Exception ex) {
            Logger.getLogger(PartidoService.class.getName()).log(Level.SEVERE, "Error al obtener lista de marcadores", ex);
            return new Respuesta(false, "Ocurrio un error durante la consulta", "");

        }
    }

    private static java.util.List<cr.ac.una.canchasws.controller.MarcadorDto> getMarcadorIdEq_1(java.lang.Long idEq) {
        cr.ac.una.canchasws.controller.MarcadorController_Service service = new cr.ac.una.canchasws.controller.MarcadorController_Service();
        cr.ac.una.canchasws.controller.MarcadorController port = service.getMarcadorControllerPort();
        return port.getMarcadorIdEq(idEq);
    }

    private static java.util.List<cr.ac.una.canchasws.controller.MarcadorDto> getMarcadorIdPar(java.lang.Long idPar) {
        cr.ac.una.canchasws.controller.MarcadorController_Service service = new cr.ac.una.canchasws.controller.MarcadorController_Service();
        cr.ac.una.canchasws.controller.MarcadorController port = service.getMarcadorControllerPort();
        return port.getMarcadorIdPar(idPar);
    }
}
