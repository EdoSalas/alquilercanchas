/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.service;

import alquilercanchas.model.Cancha;
import alquilercanchas.model.Contacto;
import alquilercanchas.model.Equipo;
import alquilercanchas.model.Usuario1;
import alquilercanchas.util.Respuesta;
import cr.ac.una.canchasws.controller.CanchaDto;
import cr.ac.una.canchasws.controller.ContactoDto;
import cr.ac.una.canchasws.controller.EquipoDto;
import cr.ac.una.canchasws.controller.UsuarioDto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matami
 */
public class UsuarioService {

    /*
    Conculta el usuario en la base de datos para saber si existe o no
    El metodo es para la hora del registro de nuevos usuarios y verificar que sea usuario unico
     */
    public Boolean getUser(String usuario) {
        try {
            Boolean us = validarUsuario_1(usuario);
            if (us == false) {
                return false;
            }
            return true;
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al obtener el usario :" + usuario + " ", ex);
            return false;
        }
    }

    public Respuesta getUsuario(String usuario, String clave) {
        try {
            Respuesta res = new Respuesta();
            UsuarioDto us = (UsuarioDto) getUsuario_1(usuario, clave);
            if (us == null) {
                return new Respuesta(false, "No se a podido acceder al usuario", "");
            }
            Usuario1 usuario1 = new Usuario1();
            for (CanchaDto c : us.getCanchas()) {
                Cancha can = new Cancha(c);
                usuario1.getCanchas().add(can);
            }
            if (us.getEquipo() != null) {
                Equipo eq = new Equipo(us.getEquipo());
                usuario1.setEquipo(eq);
            }
            return new Respuesta(true, "", "", "Usuario", new Usuario1(us));
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al obtener el usario :" + usuario + " ", ex);
            return new Respuesta(false, "Error al cargar el usuario. ", "getUusario " + ex.getMessage());
        }
    }

    public Respuesta guardarUsuario(Usuario1 us) {
        try {

            if (us == null) {
                return new Respuesta(false, "", "");
            }
            //cargado de los datos 
            UsuarioDto usuarioDto = new UsuarioDto();
            usuarioDto.setId(us.getId());
            usuarioDto.setActivo(us.getActivo());
            usuarioDto.setNombreEquipo(us.getNombreEquipo());
            usuarioDto.setTipoUsuario(us.getTipoUsuario());
            usuarioDto.setUsuario(us.getUsuario());
            usuarioDto.setClave(us.getClave());
            usuarioDto.setNombre(us.getNombre());
            if (us.getTipoUsuario() == "C" && !us.getCanchas().isEmpty()) {

                for (Cancha can : us.getCanchas()) {
                    //crear dto tipo cancha
                    CanchaDto canDto = new CanchaDto();
                    canDto.setAbierto(can.getAbierto());
                    canDto.setCerrado(can.getCerrado());
                    canDto.setDireccion(can.getDireccion());
                    canDto.setLat(Integer.valueOf(can.getLat()));
                    canDto.setLon(Integer.valueOf(can.getLon()));
                    canDto.setNombre(can.getNombre());
                    canDto.setNumeroJugadores(can.getNumeroJugadores());
                    canDto.setNumeroTelefono(can.getNumeroTelefono());
                    canDto.setPrecioDia(can.getPrecioDia());
                    canDto.setPrecioNoche(can.getPrecioNoche());
                    canDto.setTitulo(can.getTitulo());
                    usuarioDto.getCanchas().add(canDto);
                }
            } else {
                if (us.getEquipo() != null) {
                    Equipo equi = us.getEquipo();
                    //crar dto de equipo
                    EquipoDto eq = new EquipoDto();
                    eq.setGoles(equi.getGoles());
                    eq.setNombre(equi.getNombre());
                    eq.setPuntos(equi.getPuntos());
                    eq.setRendimiento(equi.getRendimiento());
                    for (Contacto cont : us.getEquipo().getIntegrantes()) {
                        ContactoDto contDto = new ContactoDto();
                        contDto.setNombre(cont.getNombre());
                        contDto.setTelefono(Integer.valueOf(cont.getTelefono()));
                    }
                    usuarioDto.setEquipo(eq);
                }
            }
            cr.ac.una.canchasws.controller.Respuesta res = guardarUsuario_1(usuarioDto);
            if (res.isEstado()) {
                UsuarioDto su = (UsuarioDto)res.getResultado();
                System.out.println(su.getNombre());
                return new Respuesta(true, "", "", "Usuario", us);
            } else {
                return new Respuesta(false, "Ocurrio un error durante el guardado", "");
            }
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Error al obtener el usario :" + us.getNombre() + " ", ex);
            return new Respuesta(false, "Ocurrio un error durante el guardado", "");
        }
    }

    private static UsuarioDto getUsuario_1(java.lang.String usuario, java.lang.String clave) {
        cr.ac.una.canchasws.controller.UsuarioController_Service service = new cr.ac.una.canchasws.controller.UsuarioController_Service();
        cr.ac.una.canchasws.controller.UsuarioController port = service.getUsuarioControllerPort();
        return port.getUsuario(usuario, clave);
    }

    private static Boolean validarUsuario_1(java.lang.String usuario) {
        cr.ac.una.canchasws.controller.UsuarioController_Service service = new cr.ac.una.canchasws.controller.UsuarioController_Service();
        cr.ac.una.canchasws.controller.UsuarioController port = service.getUsuarioControllerPort();
        return port.validarUsuario(usuario);
    }

    private static cr.ac.una.canchasws.controller.Respuesta guardarUsuario_1(cr.ac.una.canchasws.controller.UsuarioDto usuario) {
        cr.ac.una.canchasws.controller.UsuarioController_Service service = new cr.ac.una.canchasws.controller.UsuarioController_Service();
        cr.ac.una.canchasws.controller.UsuarioController port = service.getUsuarioControllerPort();
        return port.guardarUsuario(usuario);
    }

}
