/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilercanchas.service;

import alquilercanchas.model.Contacto;
import alquilercanchas.model.Equipo;
import alquilercanchas.model.Partido;
import alquilercanchas.util.Respuesta;
import cr.ac.una.canchasws.controller.ContactoDto;
import cr.ac.una.canchasws.controller.EquipoDto;
import cr.ac.una.canchasws.controller.PartidoDto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matami
 */
public class EquipoService {

    public Respuesta getEquipoIdMarc1(Long idMarc) {
        try {

            if (idMarc == null || idMarc <= 0) {
                return new Respuesta(false, "Favor ingrear los parametros de busqueda", "");
            }
            EquipoDto eqDto = getEquipoIdMarc(idMarc);
            Equipo eq = new Equipo(eqDto);
            for (ContactoDto cont : eqDto.getIntegrantes()) {
                Contacto con = new Contacto(cont);
                eq.getIntegrantes().add(con);
            }
            return new Respuesta(true, "", "", "canchas", eq);

        } catch (Exception ex) {
            Logger.getLogger(PartidoService.class.getName()).log(Level.SEVERE, "Error al obtener el equipo", ex);
            return new Respuesta(false, "Ocurrio un error durante la consulta", "");

        }
    }

    private static EquipoDto getEquipoIdMarc(java.lang.Long idMarc) {
        cr.ac.una.canchasws.controller.EquipoController_Service service = new cr.ac.una.canchasws.controller.EquipoController_Service();
        cr.ac.una.canchasws.controller.EquipoController port = service.getEquipoControllerPort();
        return port.getEquipoIdMarc2(idMarc);
    }

}
